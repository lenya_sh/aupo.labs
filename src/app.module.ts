import {Module} from '@nestjs/common';
import {ConfigModule, ConfigService} from '@nestjs/config';
import * as Joi from '@hapi/joi';
import {DatabaseModule} from './database/database.module';
import {UsersModule} from './users/users.module';
import {AppController} from './app.controller';
import {AuthenticationModule} from './authentication/authentication.module';
import {ScheduleModule} from '@nestjs/schedule';
import {BullModule} from "@nestjs/bull";
import {ClassModule} from "./class/class.module";
import {EducationLevelModule} from "./educationLevel/educationLevel.module";
import {ParallelModule} from "./parallel/parallel.module";
import {StudentModule} from "./students/student.module";
import {StudyYearModule} from "./studyYear/studyYear.module";
import {TeacherModule} from "./teacher/teacher.module";

@Module({
  imports: [
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        MYSQL_HOST: Joi.string().required(),
        MYSQL_PORT: Joi.number().required(),
        MYSQL_USER: Joi.string().required(),
        MYSQL_PASSWORD: Joi.string().required(),
        MYSQL_DATABASE: Joi.string().required(),
        JWT_SECRET: Joi.string().required(),
        JWT_EXPIRATION: Joi.string().required(),
        JWT_REFRESH_SECRET: Joi.string().required(),
        JWT_REFRESH_EXPIRATION: Joi.string().required(),
      }),
    }),
    BullModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        redis: {
          host: configService.get('REDIS_HOST'),
          port: configService.get('REDIS_PORT'),
        },
        defaultJobOptions: {
          removeOnComplete: true,
          removeOnFail: false
        },
        limiter: {
          max: 10000,
          duration: 1000,
          bounceBack: false
        }
      }),
    }),
    ScheduleModule.forRoot(),
    DatabaseModule,
    UsersModule,
    ClassModule,
    EducationLevelModule,
    ParallelModule,
    StudentModule,
    StudyYearModule,
    TeacherModule,
    AuthenticationModule,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}