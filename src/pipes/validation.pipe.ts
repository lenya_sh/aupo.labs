import { ArgumentMetadata, PipeTransform } from "@nestjs/common";
import { validate } from "class-validator";
import { plainToClass } from 'class-transformer';
import { CustomException } from "../exceptions/custom.exception";

export class CustomValidationPipe implements PipeTransform {
    public async transform(value: any, metaData: ArgumentMetadata) {
        const { metatype } = metaData; 

        const object = plainToClass(metatype!, value);
        const errors = await validate(object);

        if (errors.length) {
            throw CustomException.invalidData(errors);
        }

        return value;
    }
}