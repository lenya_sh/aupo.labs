import { ApiProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsDefined, IsInt } from "class-validator";
import { UserEntity } from "../user.entity";

export namespace GetUserDTO {
	export class GetUserRequest {
		@IsDefined({
			message: 'Id is required'
		})
		@Type(() => Number)
		@IsInt({
			message: 'Id must be an int'
		})
		@ApiProperty()
		id: number;

		constructor(id: number) {
			this.id = id;
		}
	}

	export class GetUserResponse {
		@ApiProperty()
		user: UserEntity;
		
		constructor(user: UserEntity) {
			this.user = user;
		}
	}
}