import { ApiProperty } from "@nestjs/swagger";
import { IsBoolean, IsDefined, IsIn, IsOptional, IsString, Matches } from "class-validator";
import MyRegexp from "myregexp";
import { UserRole } from "../../constants";
import { UserEntity } from "../user.entity";

export namespace CreateUserDTO {
	export class CreateUserRequest {
		@IsDefined({
			message: 'Phone is required'
		})
		@IsString({
			message: 'Phone must be a string'
		})
		@Matches(MyRegexp.phone(), {
			message: 'Phone must be a valid phone number'
		})
		@ApiProperty()
		phone: string;

		@IsOptional()
		@IsBoolean()
		active?: boolean;

		@IsOptional({
			message: 'Username is optional'
		})
		@IsString({
			message: 'Username must be a string'
		})
		@ApiProperty()
		username?: string;

		@IsOptional({
			message: 'Role is optional'
		})
		@IsString({
			message: 'Role must be a string'
		})
		@IsIn(Object.values(UserRole), {
			message: 'Role must be a valid user'
		})
		@ApiProperty()
		role?: UserRole;

		constructor(
			phone: string,
			username?: string,
			role?: UserRole,
			active?: boolean,
		) {
			this.phone = phone;
			this.username = username;
			this.role = role;
			this.active = active;
		}
	}

	export class CreateUserResponse {
		@ApiProperty()
		user: UserEntity;

		constructor(
			user: UserEntity
		) {
			this.user = user;
		}
	}
}