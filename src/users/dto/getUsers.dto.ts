﻿import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsArray, IsBoolean, IsIn, IsInt, IsOptional, IsString } from "class-validator";
import { Constants, UserRole } from "src/constants";
import { UserEntity } from "../user.entity";
import { ApiModelProperty } from "@nestjs/swagger/dist/decorators/api-model-property.decorator";

export namespace GetUsersDTO {
	export class GetUsersRequest {
		@IsOptional()
		@ApiPropertyOptional()
		@Type(() => Boolean)
		@IsBoolean()
		public active?: boolean;
	
		constructor(active?: boolean) {
			this.active = active;
		}
	}

	export class GetUsersResponse {
		@ApiProperty({type: [UserEntity]})
		users: UserEntity[];

		@ApiProperty()
		count: number;

		constructor(users: UserEntity[], count: number) {
			this.users = users;
			this.count = count;
		}
	}
}