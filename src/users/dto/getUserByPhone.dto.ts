import { ApiProperty } from "@nestjs/swagger";
import { IsDefined, IsPhoneNumber, IsString } from "class-validator";
import { UserEntity } from "../user.entity";

export namespace GetUserByPhoneDTO {
    export class GetUserByPhoneRequest {
        @IsDefined({
			message: 'Phone is required'
		})
		@IsString({
			message: 'Phone must be a string'
		})
		@IsPhoneNumber('RU', {
			message: 'Phone must be a valid phone number'
		})
		@ApiProperty()
		phone: string;
	
		constructor(phone: string) {
			this.phone = phone;
		}
    }

    export class GetUserByPhoneResponse {
        @ApiProperty()
        user: UserEntity;

        constructor(user: UserEntity) {
            this.user = user;
        }
    }
}