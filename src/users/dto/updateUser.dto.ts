import {UserRole} from '../../constants';
import {ApiProperty, ApiPropertyOptional} from '@nestjs/swagger';
import { IsInt, IsOptional, IsString, Matches } from "class-validator";
import {UserEntity} from '../user.entity';
import MyRegexp from "myregexp";

export namespace UpdateUserDTO {
	export class UpdateUserRequest {
		@IsOptional({
			message: 'Id is optional'
		})
		@IsInt({
			message: 'Id must be an int'
		})
		id: number;

		@IsOptional({message: 'Phone is optional'})
		@IsString({message: 'Phone must be a string'})
		@Matches(MyRegexp.phone())
		@ApiPropertyOptional()
		public phone?: string;

		@ApiPropertyOptional()
		@IsString()
		@IsOptional()
		username?: string;

		@ApiPropertyOptional()
		@IsString()
		@IsOptional()
		role?: UserRole;

		constructor(
			id: number,
			phone?: string,
			username?: string,
			role?: UserRole,
		) {
			this.id = id;
			this.phone = phone;
			this.username = username;
			this.role = role;
		}
	}

	export class UpdateUserResponse {
		@ApiProperty()
		user: UserEntity;

		constructor(user: UserEntity) {
			this.user = user;
		}
	}
}