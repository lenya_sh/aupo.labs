import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { VerificationEntity } from '../authentication/verification.entity';
import { UserRole } from '../constants';
import {
	Column,
	CreateDateColumn,
	Entity,
	OneToMany,
	PrimaryGeneratedColumn,
	UpdateDateColumn
} from "typeorm";

@Entity({ name: 'users' })
export class UserEntity {
	@ApiProperty()
	@PrimaryGeneratedColumn()
	public id!: number;

	@ApiPropertyOptional()
	@Column('varchar', { nullable: true })
	username?: string;

	@ApiPropertyOptional()
	@Column('varchar', { unique: true })
	phone!: string;

	@ApiProperty()
	@Column({
		type: 'varchar',
		length: 40,
		default: UserRole.Admin,
	})
	role!: UserRole;

	@ApiPropertyOptional()
	@Column('boolean', { name: 'active', default: false })
	public active!: boolean;

	@OneToMany(() => VerificationEntity, verification => verification.user)
	@ApiProperty()
	public verifications!: VerificationEntity[];
	
	@ApiProperty()
	@CreateDateColumn({ type: 'timestamp', name: 'created_at' })
	public readonly createdAt!: Date;

	@ApiProperty()
	@UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
	public readonly updatedAt!: Date;
}