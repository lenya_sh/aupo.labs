import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { UserRole } from "../constants";
import { Repository } from "typeorm";
import { CreateUserDTO } from "./dto/createUser.dto";
import { GetUserDTO } from "./dto/getUser.dto";
import { GetUserByPhoneDTO } from "./dto/getUserByPhone.dto";
import { GetUsersDTO } from "./dto/getUsers.dto";
import { UpdateUserDTO } from "./dto/updateUser.dto";
import { UserEntity } from "./user.entity";
import {CustomException} from "../exceptions/custom.exception";

@Injectable()
export class UsersService {
	constructor(
		@InjectRepository(UserEntity)
		private usersRepository: Repository<UserEntity>,
	) { }

	public async createUser(request: CreateUserDTO.CreateUserRequest): Promise<CreateUserDTO.CreateUserResponse> {
		const userWithPhone = await this.usersRepository.findOne({ where: { phone: request.phone } });
		if (userWithPhone) 
			throw CustomException.conflict(`User with phone ${request.phone} already exists`);

		const user = this.usersRepository.create({
			phone: request.phone,
			username: request.username,
			role: request.role || UserRole.Admin,
			active: request.active
		});
		
		const savedUser = await this.usersRepository.save(user);
		if (!savedUser) {
			throw CustomException.internalError('Error upon saving user');
		}

		return new CreateUserDTO.CreateUserResponse(savedUser);
	}

	public async getUsers(request: GetUsersDTO.GetUsersRequest): Promise<GetUsersDTO.GetUsersResponse> {
		let users: UserEntity[];
		let count: number;
		
		if (request.active) {
			[users, count] = await this.usersRepository.findAndCount({
				where: {
					active: true
				}
			})
		}
		else if (request.active == false) {
			[users, count] = await this.usersRepository.findAndCount({
				where: {
					active: false
				}
			})
		}
		else
			[users, count] = await this.usersRepository.findAndCount()
		
		return new GetUsersDTO.GetUsersResponse(users, count);
	}

	public async getUser(request: GetUserDTO.GetUserRequest): Promise<GetUserDTO.GetUserResponse> {
		let user = await this.usersRepository.findOne({ where: { id: request.id } });
		if (!user) 
			throw CustomException.invalidData(`User with id ${request.id} does not exist`);

		return new GetUserDTO.GetUserResponse(user);
	}

	public async getUserByPhone(request: GetUserByPhoneDTO.GetUserByPhoneRequest)
		: Promise<GetUserByPhoneDTO.GetUserByPhoneResponse> {
		const user = await this.usersRepository.findOne({ where: { phone: request.phone } });
		if (!user) {
			throw CustomException.invalidData(`User with phone ${request.phone} does not exist`);
		}

		return new GetUserDTO.GetUserResponse(user);
	}

	public async updateUser(request: UpdateUserDTO.UpdateUserRequest): Promise<UpdateUserDTO.UpdateUserResponse> {
		let { user } = await this.getUser(new GetUserDTO.GetUserRequest(request.id));

		if (request.username) {
			user.username = request.username;
		}
		
		if (request.phone != undefined && user.phone != request.phone) {
			const existingUser = await this.usersRepository.findOne({where: {phone: request.phone}});
			
			if (!!existingUser)
				throw CustomException.invalidData('Пользователь с таким номером уже существует');
			
			user.phone = request.phone;
		}

		if (request.role != undefined) {
			user.role = request.role;
		}
		
		const savedUser = await this.usersRepository.save(user);
		return new UpdateUserDTO.UpdateUserResponse(savedUser);
	}
}