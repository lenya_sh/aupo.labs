import {
	Body, Controller, Get, Param, Post, Put, Query, Req, UseGuards, UsePipes,
	ValidationPipe
} from "@nestjs/common";
import { ApiBody, ApiParam, ApiResponse, ApiTags } from "@nestjs/swagger";
import { JwtAuthenticationGuard } from "../authentication/jwt-authentication.guard";
import { RequestWithUser } from "../authentication/requestWithUser.interface";
import { RoleGuard } from "../authentication/role.guard";
import { UserRole } from "../constants";
import { CreateUserDTO } from "./dto/createUser.dto";
import { GetUserDTO } from "./dto/getUser.dto";
import { GetUsersDTO } from "./dto/getUsers.dto";
import { UpdateUserDTO } from "./dto/updateUser.dto";
import { UsersService } from "./users.service";

@Controller('users')
@ApiTags('Пользователи')
export class UsersController {
	constructor(private readonly usersService: UsersService) { }

	@Post()
	@UsePipes(ValidationPipe)
	@UseGuards(RoleGuard([UserRole.Admin]))
	@ApiBody({
		description: 'Создать пользователя',
		type: CreateUserDTO.CreateUserRequest,
	})
	create(@Body() body: CreateUserDTO.CreateUserRequest, @Req() req: RequestWithUser) {
		const request = new CreateUserDTO.CreateUserRequest(
			body.phone,
			body.username,
			body.role,
			body.active
		);
		
		return this.usersService.createUser(request);
	}

	@Get()
	@UsePipes(ValidationPipe)
	@UseGuards(RoleGuard(UserRole.Admin))
	@ApiResponse({
		status: 200,
		description: 'Получить список всех пользователей',
		type: GetUsersDTO.GetUsersResponse,
		isArray: true
	})
	getUsers(@Query() query: GetUsersDTO.GetUsersRequest) {
		const request = new GetUsersDTO.GetUsersRequest(query.active);
		return this.usersService.getUsers(request);
	}
	@Get(':id')
	@UseGuards(RoleGuard(UserRole.Admin))
	@ApiParam({ name: 'id', required: true, schema: { oneOf: [{type: 'string'}, {type: 'integer'}]} })
	@ApiResponse({
		description: 'Получить пользователя по айди',
		status: 200,
		type: GetUserDTO.GetUserResponse,
	})
	getUser(@Param() request: GetUserDTO.GetUserRequest) {
		return this.usersService.getUser(request);
	}

	@Put()
	@ApiBody({
		description: 'Обновить текущего пользователя',
		type: UpdateUserDTO.UpdateUserRequest,
	})
	@ApiResponse({
		status: 200,
		type: UpdateUserDTO.UpdateUserResponse,
	})
	@UseGuards(JwtAuthenticationGuard)
	updateCurrentUser(@Body() body: UpdateUserDTO.UpdateUserRequest, @Req() req: RequestWithUser) {
		return this.usersService.updateUser(new UpdateUserDTO.UpdateUserRequest(
			req.user.id,
			undefined,
			body.username,
		));
	}

	@ApiBody({
		description: 'Обновить пользователя по айди',
		type: UpdateUserDTO.UpdateUserRequest,
	})
	@ApiResponse({
		status: 200,
		type: UpdateUserDTO.UpdateUserResponse,
	})
	@ApiParam({ name: 'id', required: true, schema: { oneOf: [{type: 'string'}, {type: 'integer'}]} })
	@UseGuards(RoleGuard(UserRole.Admin))
	@Put(':id')
	updateUser(@Body() body: UpdateUserDTO.UpdateUserRequest, @Param() params: any) {
		return this.usersService.updateUser(new UpdateUserDTO.UpdateUserRequest(
			params.id,
			body.phone,
			body.username,
			body.role,
		));
	}
}