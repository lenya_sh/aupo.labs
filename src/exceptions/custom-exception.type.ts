﻿export namespace CustomExceptionType {
    export type CustomError = {
        code: string;
        status: number;
        message: string;
        details?: ErrorDetails;
    };

    export type ErrorDetails = any;
}