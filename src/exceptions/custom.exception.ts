﻿import { ApiProperty } from '@nestjs/swagger';
import { CustomExceptionType } from "./custom-exception.type";

export class CustomException extends Error{
    @ApiProperty()
    public code!: string;

    @ApiProperty()
    public message!: string;

    @ApiProperty()
    public status!: number;

    @ApiProperty()
    public details: any;

    public constructor(error: CustomExceptionType.CustomError) {
        super(error.details);
        
        this.code = error.code;
        this.message = error.message;
        this.status = error.status;
        this.details = error.details;
    }

    static invalidData(
        details?: CustomExceptionType.ErrorDetails,
    ): CustomException {
        if (details instanceof CustomException)
            return details;
        
        return new CustomException({
            code: 'invalid-data',
            message: details ?? 'Provided data is not valid',
            status: 400,
            details: details,
        });
    }

    static internalError(
        details?: CustomExceptionType.ErrorDetails,
    ): CustomException {
        if (details instanceof CustomException)
            return details;
        
        return new CustomException({
            code: 'internal-error',
            message: details ?? 'Internal error occurred',
            status: 500,
            details: details,
        });
    }

    static conflict(
        details?: CustomExceptionType.ErrorDetails,
    ): CustomException {
        if (details instanceof CustomException)
            return details;
        
        return new CustomException({
            code: 'conflict',
            message: details ?? 'Conflict occurred',
            status: 409,
            details: details,
        });
    }

    static forbidden(
        details?: CustomExceptionType.ErrorDetails,
    ): CustomException {
        if (details instanceof CustomException)
            return details;
        
        return new CustomException({
            code: 'forbidden',
            message: details ?? 'Action is forbidden',
            status: 403,
            details: details,
        });
    }
}