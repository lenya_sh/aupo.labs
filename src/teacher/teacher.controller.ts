import {Body, Controller, Delete, Get, Param, Post, Put, UseGuards} from "@nestjs/common";
import { ApiResponse, ApiTags } from "@nestjs/swagger";
import TeacherService from "./teacher.service";
import {GetTeachersDto} from "./dto/getTeacher.dto";
import GetEducationLevelRequest = GetTeachersDto.GetTeacherRequest;
import {CreateTeacherDto} from "./dto/createTeacher.dto";
import {JwtAuthenticationGuard} from "../authentication/jwt-authentication.guard";
import {UpdateTeacherDto} from "./dto/updateTecher.dto";
import {DeleteTeacherDto} from "./dto/deleteTeacher.dto";
import CreateTeacherRequest = CreateTeacherDto.CreateTeacherRequest;
import UpdateTeacherRequest = UpdateTeacherDto.UpdateTeacherRequest;
import CreateTeacherResponse = CreateTeacherDto.CreateTeacherResponse;
import GetTeacherResponse = GetTeachersDto.GetTeacherResponse;
import GetTeachersResponse = GetTeachersDto.GetTeachersResponse;
import DeleteTeacherResponse = DeleteTeacherDto.DeleteTeacherResponse;
import DeleteTeacherRequest = DeleteTeacherDto.DeleteTeacherRequest;
import {UpdateParallelDto} from "../parallel/dto/updateParallel.dto";
import UpdateParallelResponse = UpdateParallelDto.UpdateParallelResponse;
import {RoleGuard} from "../authentication/role.guard";
import {UserRole} from "../constants";
import UpdateTeachersResponse = UpdateTeacherDto.UpdateTeacherResponse;

@Controller('teachers')
@ApiTags('Учителя')
export class TeacherController {
    constructor(private teacherService: TeacherService) { }
    
    @Get()
    @ApiResponse({
		description: 'Получить список учителей',
		status: 200,
		type: GetTeachersResponse,
	})
    @UseGuards(JwtAuthenticationGuard)
    public async getTeachers() {
        return this.teacherService.getTeachers();
    }

    @Get(':id')
    @ApiResponse({
        description: 'Получить учителя по id',
        status: 200,
        type: GetTeacherResponse,
    })
    @UseGuards(JwtAuthenticationGuard)
    public async getTeacher(@Param() param: any) {
        const request = new GetEducationLevelRequest(param.id);
        return this.teacherService.getTeacher(request);
    }

    @Post()
    @ApiResponse({
        description: 'Создать учителя',
        status: 200,
        type: CreateTeacherResponse,
    })
    @UseGuards(RoleGuard(UserRole.Admin))
    public async createTeacher(@Body() body: CreateTeacherRequest) {
        return this.teacherService.createTeacher(body);
    }

    @Put(':id')
    @ApiResponse({
		description: 'Обновить учителя по id',
		status: 200,
		type: UpdateTeachersResponse,
    })
    @UseGuards(RoleGuard(UserRole.Admin))
    public async updateTeacher(@Param() param: any, @Body() body: UpdateTeacherRequest) {
        const request = new UpdateTeacherRequest(param.id, body.firstName, body.lastName, body.patronymic);
        return this.teacherService.updateTeacher(request);
    }

    @Delete(':id')
    @ApiResponse({
        description: 'Удалить учителя по id',
        status: 200,
        type: DeleteTeacherResponse,
    })
    @UseGuards(RoleGuard(UserRole.Admin))
    public async deleteTeacher(@Param() param: any) {
        const request = new DeleteTeacherRequest(param.id);
        return this.teacherService.deleteTeacher(request);
    }
}