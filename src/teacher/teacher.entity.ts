import {ApiProperty} from '@nestjs/swagger';
import {Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn,} from 'typeorm';
import {ClassEntity} from "../class/class.entity";

@Entity({ name: 'teachers' })
export class TeacherEntity {
	@ApiProperty()
	@PrimaryGeneratedColumn()
	public id!: number;

	@ApiProperty()
	@Column('varchar', { name: 'first_name' })
	firstName!: string;

	@ApiProperty()
	@Column('varchar', { name: 'last_name' })
	lastName!: string;

	@ApiProperty()
	@Column('varchar', { name: 'patronymic', nullable: true })
	patronymic!: string;

	@OneToMany(() => ClassEntity, classes => classes.parallel)
	@ApiProperty({ type: () => ClassEntity})
	public classes!: ClassEntity[];

	@ApiProperty()
	@CreateDateColumn({ type: 'timestamp', name: 'created_at' })
	readonly createdAt!: Date;
  
	@ApiProperty()
	@UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
	readonly updatedAt!: Date;
}