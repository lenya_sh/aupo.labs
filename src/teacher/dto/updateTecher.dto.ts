﻿import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import {IsOptional, IsString} from "class-validator";
import { TeacherEntity } from "../teacher.entity";

export namespace UpdateTeacherDto {
    export class UpdateTeacherRequest {
        public teacherId: number;
        
        @IsOptional({
            message: 'First name is optional'
        })
        @IsString({
            message: 'First name must be a string'
        })
        @ApiPropertyOptional({example: "Формат: строка длиной от 2 до 50 символов"})
        public firstName?: string;

        @IsOptional({
            message: 'Last name is optional'
        })
        @IsString({
            message: 'Last name must be a string'
        })
        @ApiPropertyOptional({example: "Формат: строка длиной от 2 до 50 символов"})
        public lastName?: string;

        @IsOptional({
            message: 'Patronymic is optional'
        })
        @IsString({
            message: 'Patronymic must be a string'
        })
        @ApiPropertyOptional({example: "Формат: строка длиной от 2 до 50 символов"})
        public patronymic?: string;

        constructor(teacherId: number, firstName?: string, lastName?: string, patronymic?: string) {
            this.teacherId = teacherId;
            this.firstName = firstName;
            this.lastName = lastName;
            this.patronymic = patronymic;
        }
    }

    export class UpdateTeacherResponse {
        @ApiProperty()
        public teacher: TeacherEntity;

        constructor(teacher: TeacherEntity) {
            this.teacher = teacher;
        }
    }
}