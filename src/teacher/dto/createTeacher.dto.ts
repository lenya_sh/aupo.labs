﻿import { ApiProperty, ApiPropertyOptional } from "@nestjs/swagger";
import {IsDefined, IsOptional, IsString} from "class-validator";
import {TeacherEntity} from "../teacher.entity";

export namespace CreateTeacherDto {
    export class CreateTeacherRequest {
        @IsDefined({
            message: 'First name is defined'
        })
        @IsString({
            message: 'First name must be a string'
        })
        @ApiProperty({example: "Формат: строка длиной от 2 до 50 символов"})
        public firstName: string;

        @IsDefined({
            message: 'Last name is defined'
        })
        @IsString({
            message: 'Last name must be a string'
        })
        @ApiProperty({example: "Формат: строка длиной от 2 до 50 символов"})
        public lastName: string;

        @IsOptional({
            message: 'Patronymic is defined'
        })
        @IsString({
            message: 'Patronymic must be a string'
        })
        @ApiPropertyOptional({example: "Формат: строка длиной от 2 до 50 символов"})
        public patronymic?: string;

        constructor(firstName: string, lastName: string, patronymic?: string) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.patronymic = patronymic;
        }
    }

    export class CreateTeacherResponse {
        @ApiProperty()
        public teacher: TeacherEntity;

        constructor(teacher: TeacherEntity) {
            this.teacher = teacher;
        }
    }
}