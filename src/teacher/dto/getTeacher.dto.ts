﻿import { ApiProperty } from "@nestjs/swagger";
import {IsDefined, IsInt} from "class-validator";
import { TeacherEntity } from "../teacher.entity";

export namespace GetTeachersDto {
    export class GetTeacherRequest {
        @IsDefined({
            message: 'Teacher id is defined'
        })
        @IsInt({
            message: 'Teacher id must be an int'
        })
        @ApiProperty()
        public teacherId: number;

        constructor(teacherId: number) {
            this.teacherId = teacherId;
        }
    }
    
    export class GetTeachersResponse {
        @ApiProperty()
        public teachers: TeacherEntity[];
        
        constructor(teachers: TeacherEntity[]) {
            this.teachers = teachers;
        }
    }

    export class GetTeacherResponse {
        @ApiProperty()
        public teacher: TeacherEntity;

        constructor(teacher: TeacherEntity) {
            this.teacher = teacher;
        }
    }
}