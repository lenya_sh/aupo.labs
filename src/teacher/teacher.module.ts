import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TeacherController } from './teacher.controller';
import { TeacherEntity } from './teacher.entity';
import TeacherService from './teacher.service';

@Module({
  imports: [TypeOrmModule.forFeature([TeacherEntity])],
  exports: [TeacherService],
  providers: [TeacherService],
  controllers: [TeacherController]
})
export class TeacherModule {}
