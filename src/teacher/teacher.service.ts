import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {TeacherEntity} from './teacher.entity';
import {CreateTeacherDto} from "./dto/createTeacher.dto";
import {CustomException} from "../exceptions/custom.exception";
import {GetTeachersDto} from "./dto/getTeacher.dto";
import {UpdateTeacherDto} from "./dto/updateTecher.dto";
import {DeleteTeacherDto} from "./dto/deleteTeacher.dto";
import GetTeacherRequest = GetTeachersDto.GetTeacherRequest;
import GetTeacherResponse = GetTeachersDto.GetTeacherResponse;
import GetTeachersResponse = GetTeachersDto.GetTeachersResponse;
import CreateTeacherRequest = CreateTeacherDto.CreateTeacherRequest;
import CreateTeacherResponse = CreateTeacherDto.CreateTeacherResponse;
import UpdateTeacherRequest = UpdateTeacherDto.UpdateTeacherRequest;
import DeleteTeacherResponse = DeleteTeacherDto.DeleteTeacherResponse;
import UpdateTeacherResponse = UpdateTeacherDto.UpdateTeacherResponse;
import DeleteTeacherRequest = DeleteTeacherDto.DeleteTeacherRequest;
import { NameChecker } from "../utils/nameChecker";

@Injectable()
export default class TeacherService {
    constructor(
        @InjectRepository(TeacherEntity)
        private readonly teachersRepository: Repository<TeacherEntity>
    ) { }

    public async getTeacher(request: GetTeacherRequest): Promise<GetTeacherResponse> {
        const teacher = await this.teachersRepository.findOne({
            where: {
                id: request.teacherId
            }
        });
        
        if (!teacher)
            throw CustomException.invalidData(`Учитель с id ${request.teacherId} не найден`);
        
        return new GetTeacherResponse(teacher);
    }

    public async getTeachers(): Promise<GetTeachersResponse> {
        const teachers = await this.teachersRepository.find();
        return new GetTeachersResponse(teachers);
    }

    public async createTeacher(request: CreateTeacherRequest): Promise<CreateTeacherResponse> {
        await NameChecker.check(request.firstName, 'names');
        await NameChecker.check(request.lastName, 'names');
        if (request.patronymic)
            await NameChecker.check(request.patronymic, 'names');
        
        const createdTeacher = await this.teachersRepository.create({
            firstName: request.firstName,
            lastName: request.lastName,
            patronymic: request.patronymic
        });
        
        const savedTeacher = await this.teachersRepository.save(createdTeacher);
        if (!savedTeacher)
          throw CustomException.internalError('Error upon creating teacher');
        
        return new CreateTeacherResponse(savedTeacher);
    }

    public async updateTeacher(request: UpdateTeacherRequest): Promise<UpdateTeacherResponse> {
        const {teacher} = await this.getTeacher(new GetTeacherRequest(request.teacherId));
        
        if (request.firstName) {
            await NameChecker.check(request.firstName, 'names');
            teacher.firstName = request.firstName;
        }
        if (request.lastName) {
            await NameChecker.check(request.lastName, 'names');
            teacher.lastName = request.lastName;
        }
        if (request.patronymic) {
            await NameChecker.check(request.patronymic, 'names');
            teacher.patronymic = request.patronymic;
        }
        
        const savedTeacher = await this.teachersRepository.save(teacher);
        if (!savedTeacher)
            throw CustomException.internalError('Error upon updating teacher');
        
        return new UpdateTeacherResponse(savedTeacher);
    }

    public async deleteTeacher(request: DeleteTeacherRequest): Promise<DeleteTeacherResponse> {
        await this.teachersRepository.delete(request.levelId);
        return new DeleteTeacherResponse(true);
    }
}