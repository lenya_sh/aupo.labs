import {ApiProperty} from '@nestjs/swagger';
import {
	Column,
	CreateDateColumn,
	Entity, JoinColumn, ManyToOne,
	OneToMany,
	PrimaryGeneratedColumn,
	UpdateDateColumn
} from "typeorm";
import {ClassEntity} from "../class/class.entity";
import { EducationLevelEntity } from "../educationLevel/educationLevel.entity";

@Entity({ name: 'parallels' })
export class ParallelEntity {
	@PrimaryGeneratedColumn()
	@ApiProperty()
	id!: number;

	@ApiProperty()
	@Column('int', { name: 'parallel_value', unique: true })
	parallelValue!: number;

	@ApiProperty()
	@Column({ name: 'education_level_id', type: 'int' })
	public educationLevelId!: number;

	@ManyToOne(() => EducationLevelEntity, grade => grade.parallels)
	@JoinColumn({ name: 'education_level_id' })
	@ApiProperty({ type: () => EducationLevelEntity})
	public educationLevel!: EducationLevelEntity;
	
	@OneToMany(() => ClassEntity, classes => classes.parallel)
	@ApiProperty({ type: () => ClassEntity})
	public classes!: ClassEntity[];

	@ApiProperty()
	@CreateDateColumn({ type: 'timestamp', name: 'created_at' })
	readonly createdAt!: Date;
  
	@ApiProperty()
	@UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
	readonly updatedAt!: Date;
}