﻿import { ApiProperty } from "@nestjs/swagger";
import { IsDefined, IsInt } from "class-validator";
import { ParallelEntity } from "../parallel.entity";

export namespace CreateParallelDto {
  export class CreateParallelRequest {
    @IsDefined({
      message: 'Parallel is defined'
    })
    @IsInt()
    @ApiProperty()
    public parallel: number;

    @IsDefined({
      message: 'Education level id is defined'
    })
    @IsInt()
    @ApiProperty()
    public educationLevelId: number;

    constructor(parallel: number, educationLevelId: number) {
      this.parallel = parallel;
      this.educationLevelId = educationLevelId;
    }
  }

  export class CreateParallelResponse {
    @ApiProperty()
    public parallel: ParallelEntity;

    constructor(parallel: ParallelEntity) {
      this.parallel = parallel;
    }
  }
}