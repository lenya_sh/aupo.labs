﻿import { ApiProperty } from "@nestjs/swagger";
import {IsDefined} from "class-validator";

export namespace DeleteParallelDto {
    export class DeleteParallelRequest {
        @IsDefined ({ message: 'Parallel id is required'})
        @ApiProperty()
        public parallelId: number;

        constructor(parallelId: number) {
            this.parallelId = parallelId;
        }
    }

    export class DeleteParallelResponse {
        @ApiProperty()
        public status: boolean;

        constructor(status: boolean) {
            this.status = status;
        }
    }
}