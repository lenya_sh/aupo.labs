﻿import { ApiProperty } from "@nestjs/swagger";
import { IsDefined, IsInt, IsOptional } from "class-validator";
import { ParallelEntity } from "../parallel.entity";

export namespace UpdateParallelDto {
    export class UpdateParallelRequest {
        public parallelId: number;
        
        @IsOptional({
            message: 'Parallel value is optional'
        })
        @IsInt()
        @ApiProperty()
        public parallel?: number;

        @IsOptional({
            message: 'Education level id is optional'
        })
        @IsInt()
        @ApiProperty()
        public educationLevelId?: number;

        constructor(parallelId: number, parallel?: number, educationLevelId?: number) {
            this.parallelId = parallelId;
            this.parallel = parallel;
            this.educationLevelId = educationLevelId;
        }
    }

    export class UpdateParallelResponse {
        @ApiProperty()
        public parallel: ParallelEntity;

        constructor(parallel: ParallelEntity) {
            this.parallel = parallel;
        }
    }
}