﻿import { ApiProperty } from "@nestjs/swagger";
import {IsDefined, IsInt} from "class-validator";
import { ParallelEntity } from "../parallel.entity";

export namespace GetParallelDto {
    export class GetParallelRequest {
        @IsDefined({
            message: 'Parallel id is defined'
        })
        @IsInt({
            message: 'Parallel id must be an int'
        })
        @ApiProperty()
        public parallelId: number;

        constructor(parallelId: number) {
            this.parallelId = parallelId;
        }
    }
    
    export class GetParallelsResponse {
        @ApiProperty()
        public parallels: ParallelEntity[];
        
        constructor(parallels: ParallelEntity[]) {
            this.parallels = parallels;
        }
    }

    export class GetParallelResponse {
        @ApiProperty()
        public parallel: ParallelEntity;

        constructor(parallel: ParallelEntity) {
            this.parallel = parallel;
        }
    }
}