import {Body, Controller, Delete, Get, Param, Post, Put, UseGuards} from "@nestjs/common";
import { ApiResponse, ApiTags } from "@nestjs/swagger";
import ParallelService from "./parallel.service";
import {GetParallelDto} from "./dto/getParallel.dto";
import { CreateParallelDto} from "./dto/createParallel.dto";
import {JwtAuthenticationGuard} from "../authentication/jwt-authentication.guard";
import {UpdateParallelDto} from "./dto/updateParallel.dto";
import {DeleteParallelDto} from "./dto/deleteParallel.dto";
import CreateParallelRequest = CreateParallelDto.CreateParallelRequest;
import CreateParallelResponse = CreateParallelDto.CreateParallelResponse;
import GetParallelResponse = GetParallelDto.GetParallelResponse;
import GetParallelsResponse = GetParallelDto.GetParallelsResponse;
import GetParallelRequest = GetParallelDto.GetParallelRequest;
import UpdateParallelRequest = UpdateParallelDto.UpdateParallelRequest;
import UpdateParallelResponse = UpdateParallelDto.UpdateParallelResponse;
import DeleteParallelRequest = DeleteParallelDto.DeleteParallelRequest;
import {RoleGuard} from "../authentication/role.guard";
import {UserRole} from "../constants";

@Controller('parallels')
@ApiTags('Параллель')
export class ParallelController {
    constructor(
        private parallelService: ParallelService
    ) { }
    
    @Get()
    @ApiResponse({
		description: 'Получить список параллелей',
		status: 200,
		type: GetParallelsResponse,
	})
    @UseGuards(JwtAuthenticationGuard)
    public async getParallels() {
        return this.parallelService.getParallels();
    }

    @Get(':id')
    @ApiResponse({
        description: 'Получить параллель по id',
        status: 200,
        type: GetParallelResponse,
    })
    @UseGuards(JwtAuthenticationGuard)
    public async getParallel(@Param() param: any) {
        const request = new GetParallelRequest(param.id);
        return this.parallelService.getParallel(request);
    }

    @Post()
    @ApiResponse({
        description: 'Создать параллель',
        status: 200,
        type: CreateParallelResponse,
    })
    @UseGuards(RoleGuard(UserRole.Admin))
    public async createParallel(@Body() body: CreateParallelRequest) {
        return this.parallelService.createParallel(body);
    }

    @Put(':id')
    @ApiResponse({
		description: 'Обновить параллель по id',
		status: 200,
		type: UpdateParallelResponse,
    })
    @UseGuards(RoleGuard(UserRole.Admin))
    public async updateParallel(@Param() param: any, @Body() body: UpdateParallelRequest) {
        const request = new UpdateParallelRequest(param.id, body.parallel, body.educationLevelId);
        return this.parallelService.updateParallel(request);
    }

    @Delete(':id')
    @ApiResponse({
        description: 'Удалить параллель по id',
        status: 200,
        type: DeleteParallelRequest,
    })
    @UseGuards(RoleGuard(UserRole.Admin))
    public async deleteParallel(@Param() param: any) {
        const request = new DeleteParallelRequest(param.id);
        return this.parallelService.deleteParallel(request);
    }
}