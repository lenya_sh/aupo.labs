import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ParallelEntity } from './parallel.entity';
import ParallelService from './parallel.service';
import {ParallelController} from "./parallel.controller";
import { EducationLevelModule } from "../educationLevel/educationLevel.module";

@Module({
  imports: [
      TypeOrmModule.forFeature([ParallelEntity]),
      EducationLevelModule
  ],
  exports: [ParallelService],
  providers: [ParallelService],
  controllers: [ParallelController]
})
export class ParallelModule {}
