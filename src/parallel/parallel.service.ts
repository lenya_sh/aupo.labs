import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {ParallelEntity} from './parallel.entity';
import {CreateParallelDto} from "./dto/createParallel.dto";
import {CustomException} from "../exceptions/custom.exception";
import {GetParallelDto} from "./dto/getParallel.dto";
import {UpdateParallelDto} from "./dto/updateParallel.dto";
import {DeleteParallelDto} from "./dto/deleteParallel.dto";
import CreateParallelRequest = CreateParallelDto.CreateParallelRequest;
import CreateParallelResponse = CreateParallelDto.CreateParallelResponse;
import GetParallelRequest = GetParallelDto.GetParallelRequest;
import GetParallelResponse = GetParallelDto.GetParallelResponse;
import GetParallelsResponse = GetParallelDto.GetParallelsResponse;
import UpdateParallelRequest = UpdateParallelDto.UpdateParallelRequest;
import UpdateParallelResponse = UpdateParallelDto.UpdateParallelResponse;
import DeleteParallelRequest = DeleteParallelDto.DeleteParallelRequest;
import DeleteParallelResponse = DeleteParallelDto.DeleteParallelResponse;
import EducationLevelService from "../educationLevel/educationLevel.service";
import { GetEducationLevelDto } from "../educationLevel/dto/getEducationLevel.dto";
import GetEducationLevelRequest = GetEducationLevelDto.GetEducationLevelRequest;

@Injectable()
export default class ParallelService {
    private readonly templateRegex;
    constructor(
        @InjectRepository(ParallelEntity)
        private readonly parallelRepository: Repository<ParallelEntity>,
        private readonly educationLevelService: EducationLevelService,
    ) {
        this.templateRegex = new RegExp('^[1-9][0-1]?[а-яА-я]$');
    }

    public async getParallel(request: GetParallelRequest): Promise<GetParallelResponse> {
        const parallel = await this.parallelRepository.findOne({
            where: {
                id: request.parallelId
            }
        });
        
        if (!parallel)
            throw CustomException.invalidData(`Параллель с id ${request.parallelId} не найден`);
        
        return new GetParallelResponse(parallel);
    }

    public async getParallels(): Promise<GetParallelsResponse> {
        const parallels = await this.parallelRepository.find();
        return new GetParallelsResponse(parallels);
    }

    public async createParallel(request: CreateParallelRequest): Promise<CreateParallelResponse> {
        await this.checkParallelValue(request.parallel);
        await this.educationLevelService.getEducationLevel(new GetEducationLevelRequest(request.educationLevelId));
        
        const createdParallel = await this.parallelRepository.create({
            parallelValue: request.parallel,
            educationLevelId: request.educationLevelId
        });
        
        const savedParallel = await this.parallelRepository.save(createdParallel);
        if (!savedParallel)
          throw CustomException.internalError('Error upon creating parallel');
        
        return new CreateParallelResponse(savedParallel);
    }

    public async updateParallel(request: UpdateParallelRequest): Promise<UpdateParallelResponse> {
        const {parallel} = await this.getParallel(new GetParallelRequest(request.parallelId));
        
        if(request.parallel) {
            await this.checkParallelValue(request.parallel);
            parallel.parallelValue = request.parallel;   
        }
        if (request.educationLevelId) {
            await this.educationLevelService.getEducationLevel(new GetEducationLevelRequest(request.educationLevelId));
            parallel.educationLevelId = request.educationLevelId
        }
        
        const savedParallel = await this.parallelRepository.save(parallel);
        if (!savedParallel)
            throw CustomException.internalError('Error upon updating parallel');
        
        return new UpdateParallelResponse(savedParallel);
    }

    public async deleteParallel(request: DeleteParallelRequest): Promise<DeleteParallelResponse> {
        await this.parallelRepository.delete(request.parallelId);
        return new DeleteParallelResponse(true);
    }
    
    private async checkParallelValue(value: number) {
        if (value <= 0 || value > 11)
            throw CustomException.invalidData(`Значение ${value} некорректно`);
    }
}