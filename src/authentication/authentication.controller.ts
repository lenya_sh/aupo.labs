import { Body, Controller, Get, Post, Query, Req, UseGuards, UsePipes } from "@nestjs/common";
import { ApiResponse, ApiTags } from "@nestjs/swagger";
import { CustomValidationPipe } from "../pipes/validation.pipe";
import { AuthenticationService } from "./authentication.service";
import { AuthDTO } from "./dto/auth.dto";
import { ConfirmDTO } from "./dto/confirm.dto";
import { GetMeDTO } from "./dto/getMe.dto";
import { JwtAuthenticationGuard } from "./jwt-authentication.guard";
import { RequestWithUser } from "./requestWithUser.interface";

@Controller('auth')
@ApiTags('Аутентификация')
export class AuthenticationController {
	constructor(private readonly authenticationService: AuthenticationService) { }

	@Post()
	@UsePipes(CustomValidationPipe)
	@ApiResponse({
		status: 201,
		type: AuthDTO.AuthResponse,
		description: 'Получить айди верификации'
	})
	async auth(@Body() body: AuthDTO.AuthRequest) {
		return this.authenticationService.auth(body);
	}

	@Get('/confirm')
	@UsePipes(CustomValidationPipe)
	@ApiResponse({
		status: 200,
		type: ConfirmDTO.ConfirmResponse,
		description: 'Подвердить верификацию'
	})
	async confirm(@Query() query: ConfirmDTO.ConfirmRequest) {
		return this.authenticationService.confirm(query);
	}

	@Get('/me')
	@UsePipes(CustomValidationPipe)
	@UseGuards(JwtAuthenticationGuard)
	@ApiResponse({
		status: 200,
		type: GetMeDTO.GetMeResponse,
		description: 'Получения пользователеля'
	})
	async getMe(@Req() req: RequestWithUser) {
		const getMeRequest = new GetMeDTO.GetMeRequest(req.user.id);
		return this.authenticationService.getMe(getMeRequest);
	}
}