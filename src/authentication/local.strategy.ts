import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import {UsersService} from '../users/users.service';
import { GetUserByPhoneDTO } from '../users/dto/getUserByPhone.dto';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly userService: UsersService) {
        super({
            usernameField: 'verificationId',
            passwordField: 'code',
        });
    }
    async validate(phone: string): Promise<boolean> {
        const getUserByPhoneRequest = new GetUserByPhoneDTO.GetUserByPhoneRequest(phone);
        return !!(await this.userService.getUserByPhone(getUserByPhoneRequest));
    }
}
