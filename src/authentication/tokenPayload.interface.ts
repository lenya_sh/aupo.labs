import { UserRole } from '../constants';

export interface TokenPayload {
  userId: number;
  userRole: UserRole;
}
