import {CanActivate, ExecutionContext, mixin, Type} from '@nestjs/common';
import {UserRole} from '../constants';
import {RequestWithUser} from './requestWithUser.interface';
import {JwtAuthenticationGuard} from './jwt-authentication.guard';

export const RoleGuard = (roles: UserRole | UserRole[]): Type<CanActivate> => {
  class RoleGuardMixin extends JwtAuthenticationGuard {
    async canActivate(context: ExecutionContext) {
      await super.canActivate(context);

      const request = context.switchToHttp().getRequest<RequestWithUser>();
      const user = request.user;
      const availableRoles = Array.isArray(roles) ? roles : [roles];

      return user?.role == UserRole.Admin ?
          true :
          availableRoles.some((r) => user?.role.includes(r));
    }
  }

  return mixin(RoleGuardMixin);
};
