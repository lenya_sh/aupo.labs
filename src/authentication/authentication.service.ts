import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { JwtService } from "@nestjs/jwt";
import { InjectRepository } from "@nestjs/typeorm";
import { GetUserDTO } from "../users/dto/getUser.dto";
import { GetUserByPhoneDTO } from "../users/dto/getUserByPhone.dto";
import { UsersService } from "../users/users.service";
import { Repository } from "typeorm";
import { AuthDTO } from "./dto/auth.dto";
import { ConfirmDTO } from "./dto/confirm.dto";
import { GetMeDTO } from "./dto/getMe.dto";
import { TokenPayload } from "./tokenPayload.interface";
import { VerificationEntity } from "./verification.entity";
import { CodeGenerate } from "../utils/codeGenerate";
import { CustomException } from "../exceptions/custom.exception";

@Injectable()
export class AuthenticationService {

    constructor(
        @InjectRepository(VerificationEntity)
        private verificationRepository: Repository<VerificationEntity>,
        private readonly usersService: UsersService,
        private readonly jwtService: JwtService,
        private readonly configService: ConfigService
    ) {
    }
    public async confirm(request: ConfirmDTO.ConfirmRequest): Promise<ConfirmDTO.ConfirmResponse> {
        const verification = await this.verificationRepository.findOne({ where: { id: request.id } });
        if (!verification) {
            throw CustomException.invalidData(`Verification with id ${request.id} does not exist`);
        }

        const getUserByIdRequest = new GetUserDTO.GetUserRequest(verification.userId);
        const { user } = await this.usersService.getUser(getUserByIdRequest);

        if (verification.code != request.code) {
            throw CustomException.invalidData(`Код ${request.code} неверный`);
        }

        const payload: TokenPayload = {userId: user.id, userRole: user.role};
        const jwtToken = await this.jwtService.signAsync(payload, {
            secret: this.configService.get('JWT_REFRESH_SECRET'),
            expiresIn: `${this.configService.get('JWT_REFRESH_EXPIRATION')}s`,
        });

        return new ConfirmDTO.ConfirmResponse(jwtToken, user.id);
    }

    public async auth(request: AuthDTO.AuthRequest): Promise<AuthDTO.AuthResponse> {
        const {user} = await this.usersService.getUserByPhone(new GetUserByPhoneDTO.GetUserByPhoneRequest(request.phone));
        
        if (request.role && request.role != user.role)
            throw CustomException.invalidData(`Номер телефона +${user.phone} используется для пользователя с другой ролью`);

        const verification = await this.createVerification(user.id);
        
        return new AuthDTO.AuthResponse(verification.id);
    }

    public async getMe(request: GetMeDTO.GetMeRequest): Promise<GetMeDTO.GetMeResponse> {
        const getUserByIdRequest = new GetUserDTO.GetUserRequest(request.id);
        const { user } = await this.usersService.getUser(getUserByIdRequest);

        return new GetMeDTO.GetMeResponse(user);
    }

    private async createVerification(userId: number): Promise<VerificationEntity> {
        const verification = this.verificationRepository.create({
            userId: userId
        });

        verification.code = await CodeGenerate.generate(4, true);
        
        return await this.verificationRepository.save(
            verification,
        );
    }
}