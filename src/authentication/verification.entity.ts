import { ApiProperty } from '@nestjs/swagger';
import { UserEntity } from '../users/user.entity';
import {
	Column,
	CreateDateColumn,
	Entity,
	JoinColumn,
	ManyToOne,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm';

@Entity({ name: 'verifications' })
export class VerificationEntity {
	@PrimaryGeneratedColumn()
	public id!: number;

	@Column('int', { name: 'user_id' })
	@ApiProperty()
	public userId!: number;

	@ManyToOne(() => UserEntity, user => user.verifications, {})
	@JoinColumn({ name: 'user_id' })
	@ApiProperty()
	public user!: UserEntity

	@Column('varchar')
	public code!: string;

	@CreateDateColumn({ name: 'created_at', type: 'timestamp' })
	@ApiProperty()
	public createdAt!: Date;

	@UpdateDateColumn({ name: 'updated_at', type: 'timestamp' })
	@ApiProperty()
	public updatedAt!: Date;
}