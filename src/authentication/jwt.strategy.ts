import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Request } from 'express';
import {UsersService} from '../users/users.service';
import { TokenPayload } from './tokenPayload.interface';
import { GetUserDTO } from '../users/dto/getUser.dto';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
	constructor(
		private readonly configService: ConfigService,
		private readonly userService: UsersService,
	) {
		super({
			jwtFromRequest: ExtractJwt.fromExtractors([
				(request: Request) => {
					return request?.headers?.['x-auth-token'] as string;
				},
			]),
			secretOrKey: configService.get('JWT_SECRET'),
		});
	}

	async validate(payload: TokenPayload) {
		const getUserRequest = new GetUserDTO.GetUserRequest(payload.userId);
		const { user } = await this.userService.getUser(getUserRequest);
		return user;
	}
}
