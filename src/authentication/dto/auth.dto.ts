import { ApiProperty } from '@nestjs/swagger';
import {
	IsDefined, IsIn, IsOptional, IsPhoneNumber, IsString
} from 'class-validator';
import { UserRole } from 'src/constants';

export namespace AuthDTO {
	export class AuthRequest {
		@IsDefined({
			message: 'Phone is required'
		})
		@IsString({
			message: 'Phone must be a string'
		})
		@IsPhoneNumber('RU', {
			message: 'Phone must be a valid phone number'
		})
		@ApiProperty()
		phone: string;

		@IsOptional({
			message: 'Role is required'
		})
		@IsString({
			message: 'Role must be a string'
		})
		@ApiProperty()
		@IsIn(Object.values(UserRole))
		role: UserRole;
	
		constructor(phone: string, role: UserRole) {
			this.phone = phone;
			this.role = role;
		}
	}

	export class AuthResponse {
		@ApiProperty()
		verificationId: number;

		constructor(verificationId: number) {
			this.verificationId = verificationId;
		}
	}
}