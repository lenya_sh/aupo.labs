import { ApiProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsDefined, IsInt } from "class-validator";
import { UserEntity } from "../../users/user.entity";

export namespace GetMeDTO {
	export class GetMeRequest {
		@IsDefined({
			message: 'Id is required'
		})
		@Type(() => Number)
		@IsInt({
			message: 'Id must be an int'
		})
		id: number;

		constructor(id: number) {
			this.id = id;
		}
	}

	export class GetMeResponse {
		@ApiProperty()
		user: UserEntity;
		
		constructor(user: UserEntity) {
			this.user = user;
		}
	}
}