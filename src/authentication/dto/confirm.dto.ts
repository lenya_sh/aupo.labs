import { ApiProperty } from "@nestjs/swagger";
import { Type } from "class-transformer";
import { IsDefined, IsInt, IsString, Length } from "class-validator";

export namespace ConfirmDTO {
    export class ConfirmRequest {
        @IsDefined({
            message: 'Id is required'
        })
        @Type(() => Number)
        @IsInt({
            message: 'Id must be an int'
        })
        @ApiProperty()
        id: number;
    
        @IsDefined({
            message: 'Code is required'
        })
        @IsString({
            message: 'Code must be a string'
        })
        @Length(4, undefined, {
            message: 'Code is must be of length of 4'
        })
        @ApiProperty()
        code: string;
    
        constructor(id: number, code: string) {
            this.id = id;   
            this.code = code;
        }
    }

    export class ConfirmResponse {
        @ApiProperty()
        jwt: string;
        
        @ApiProperty()
        userId: number;

        constructor(jwt: string, userId: number) {
            this.jwt = jwt;
            this.userId = userId;
        }
    }
}