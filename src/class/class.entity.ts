import {ApiProperty} from '@nestjs/swagger';
import {
	Column,
	CreateDateColumn,
	Entity, JoinColumn,
	ManyToOne, OneToMany,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm';
import {ParallelEntity} from "../parallel/parallel.entity";
import {TeacherEntity} from "../teacher/teacher.entity";
import {StudyYearEntity} from "../studyYear/studyYear.entity";
import {StudentEntity} from "../students/student.entity";

@Entity({ name: 'classes' })
export class ClassEntity {
	@ApiProperty()
	@PrimaryGeneratedColumn()
	public id!: number;
	
	@ApiProperty()
	@Column({ name: 'teacher_id', type: 'int' })
	public teacherId!: number;

	@ManyToOne(() => TeacherEntity, teacher => teacher.classes)
	@JoinColumn({ name: 'teacher_id' })
	@ApiProperty({ type: () => TeacherEntity})
	public teacher!: TeacherEntity;

	@ApiProperty()
	@Column({ name: 'letter', type: 'varchar', length: 2 })
	public letter!: string;

	@ApiProperty()
	@Column({ name: 'parallel_id', type: 'int' })
	public parallelId!: number;

	@ManyToOne(() => ParallelEntity, parallel => parallel.classes)
	@JoinColumn({ name: 'parallel_id' })
	@ApiProperty({ type: () => ParallelEntity})
	public parallel!: ParallelEntity;

	@ApiProperty()
	@Column({ name: 'study_year_id', type: 'int' })
	public studyYearId!: number;

	@ManyToOne(() => StudyYearEntity, studyYear => studyYear.classes)
	@JoinColumn({ name: 'study_year_id' })
	@ApiProperty({ type: () => StudyYearEntity})
	public studyYear!: StudyYearEntity;

	@OneToMany(() => StudentEntity, students => students.classEntity)
	@ApiProperty({ type: () => StudentEntity})
	public students!: StudentEntity[];

	@ApiProperty()
	@CreateDateColumn({ type: 'timestamp', name: 'created_at' })
	readonly createdAt!: Date;
  
	@ApiProperty()
	@UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
	readonly updatedAt!: Date;
}