import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClassController } from './class.controller';
import { ClassEntity } from './class.entity';
import ClassService from './class.service';
import {TeacherModule} from "../teacher/teacher.module";
import { ParallelModule } from "../parallel/parallel.module";
import { StudyYearModule } from "../studyYear/studyYear.module";
import { CsvModule } from "nest-csv-parser";

@Module({
  imports: [
      TypeOrmModule.forFeature([ClassEntity]),
      TeacherModule,
      ParallelModule,
      StudyYearModule,
      CsvModule,
  ],
  exports: [ClassService],
  providers: [ClassService],
  controllers: [ClassController]
})
export class ClassModule {}
