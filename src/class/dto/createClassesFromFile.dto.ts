﻿import {ApiProperty} from "@nestjs/swagger";
import { ClassEntity } from "../class.entity";

export namespace CreateClassesFromFileDto {
	export class CreateClassesFromFileResponse {
		@ApiProperty()
		public classes: ClassEntity[];
		
		@ApiProperty()
		public errorsMessages: string;

		constructor(classes: ClassEntity[], errorMessages: string) {
			this.classes = classes;
			this.errorsMessages	= errorMessages;
		}
	}
}