﻿import {ApiProperty, ApiPropertyOptional} from "@nestjs/swagger";
import {IsNumber, IsOptional, IsString} from "class-validator";
import { ClassEntity } from "../class.entity";

export namespace UpdateClassDto {
    export class UpdateClassRequest {
        public classId: number;
        
        @IsOptional({ message: 'Teacher id is optional'})
        @ApiPropertyOptional()
        public teacherId?: number;

        @IsOptional({
            message: 'Parallel id is optional'
        })
        @IsNumber()
        @ApiPropertyOptional()
        public parallelId?: number;

        @IsOptional({
            message: 'Study year id is optional'
        })
        @IsNumber()
        @ApiPropertyOptional()
        public studyYearId?: number;

        @IsOptional({
            message: 'Letter is optional'
        })
        @IsString()
        @ApiPropertyOptional()
        public letter?: string;
        
        constructor(classId: number, teacherId?: number, parallelId?: number, studyYearId?: number, letter?: string) {
            this.classId = classId;
            this.teacherId = teacherId;
            this.parallelId = parallelId;
            this.studyYearId = studyYearId;
            this.letter = letter;
        }
    }

    export class UpdateClassResponse {
        @ApiProperty()
        public classEntity: ClassEntity;

        constructor(classEntity: ClassEntity) {
            this.classEntity = classEntity;
        }
    }
}