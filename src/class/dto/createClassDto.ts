﻿import {ApiProperty} from "@nestjs/swagger";
import { IsDefined, IsNumber, IsString } from "class-validator";
import {ClassEntity} from "../class.entity";

export namespace CreateClassDto {
    export class CreateClassRequest {
        @IsDefined({
            message: 'Parallel id is defined'
        })
        @IsNumber()
        @ApiProperty()
        public parallelId: number;

        @IsDefined({
            message: 'Teacher id is defined'
        })
        @IsNumber()
        @ApiProperty()
        public teacherId: number;

        @IsDefined({
            message: 'Study year id is defined'
        })
        @IsNumber()
        @ApiProperty()
        public studyYearId: number;

        @IsDefined({
            message: 'Letter is defined'
        })
        @IsString()
        @ApiProperty({example: 'Одна или две маленьких русских буквы'})
        public letter: string;

        constructor(parallelId: number, teacherId: number, studyYearId: number, letter: string) {
            this.parallelId = parallelId;
            this.teacherId = teacherId;
            this.studyYearId = studyYearId;
            this.letter = letter;
        }
    }

    export class CreateClassResponse {
        @ApiProperty()
        public classEntity: ClassEntity;

        constructor(classEntity: ClassEntity) {
            this.classEntity = classEntity;
        }
    }
}