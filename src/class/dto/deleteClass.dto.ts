﻿import { ApiProperty } from "@nestjs/swagger";
import {IsDefined} from "class-validator";

export namespace DeleteClassDto {
    export class DeleteClassRequest {
        @IsDefined ({ message: 'Class id is required'})
        @ApiProperty()
        public classId: number;

        constructor(classId: number) {
            this.classId = classId;
        }
    }

    export class DeleteClassResponse {
        @ApiProperty()
        public status: boolean;

        constructor(status: boolean) {
            this.status = status;
        }
    }
}