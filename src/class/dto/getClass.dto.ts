﻿import { ApiProperty } from "@nestjs/swagger";
import {IsDefined, IsInt} from "class-validator";
import { ClassEntity } from "../class.entity";

export namespace GetClassDto {
    export class GetClassRequest {
        @IsInt({
            message: 'Class id must be an int'
        })
        @ApiProperty()
        public classId: number;

        constructor(classId: number) {
            this.classId = classId;
        }
    }
    
    export class GetClassesResponse {
        @ApiProperty()
        public classes: ClassEntity[];
        
        constructor(classes: ClassEntity[]) {
            this.classes = classes;
        }
    }

    export class GetClassResponse {
        @ApiProperty()
        public classEntity: ClassEntity;

        constructor(classEntity: ClassEntity) {
            this.classEntity = classEntity;
        }
    }
}