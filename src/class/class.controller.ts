import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
    UploadedFile,
    UseGuards,
    UseInterceptors
} from "@nestjs/common";
import {ApiResponse, ApiTags} from "@nestjs/swagger";
import ClassService from "./class.service";
import {JwtAuthenticationGuard} from "../authentication/jwt-authentication.guard";
import {GetClassDto} from "./dto/getClass.dto";
import { CreateClassDto } from "./dto/createClassDto";
import {UpdateClassDto} from "./dto/updateClass.dto";
import {DeleteClassDto} from "./dto/deleteClass.dto";
import {RoleGuard} from "../authentication/role.guard";
import {UserRole} from "../constants";
import GetClassResponse = GetClassDto.GetClassResponse;
import GetClassRequest = GetClassDto.GetClassRequest;
import CreateClassResponse = CreateClassDto.CreateClassResponse;
import CreateClassRequest = CreateClassDto.CreateClassRequest;
import UpdateClassResponse = UpdateClassDto.UpdateClassResponse;
import UpdateClassRequest = UpdateClassDto.UpdateClassRequest;
import DeleteClassResponse = DeleteClassDto.DeleteClassResponse;
import DeleteClassRequest = DeleteClassDto.DeleteClassRequest;
import { FileInterceptor } from "@nestjs/platform-express";
import { FileValidationPipe } from "../pipes/file.validator";
import { CreateClassesFromFileDto } from "./dto/createClassesFromFile.dto";
import CreateClassesFromFileResponse = CreateClassesFromFileDto.CreateClassesFromFileResponse;

@Controller('classes')
@ApiTags('Классы')
export class ClassController {
    constructor(private classService: ClassService) { }
    
    @Get()
    @ApiResponse({
		description: 'Получить список классов',
		status: 200,
		type: Get,
	})
    @UseGuards(JwtAuthenticationGuard)
    public async getClasses() {
        return this.classService.getClasses();
    }

    @Get(':id')
    @ApiResponse({
        description: 'Получить класс по id',
        status: 200,
        type: GetClassResponse,
    })
    @UseGuards(JwtAuthenticationGuard)
    public async getClass(@Param() param: any) {
        const request = new GetClassRequest(param.id);
        return this.classService.getClass(request);
    }

    @Post()
    @ApiResponse({
        description: 'Создать класс',
        status: 200,
        type: CreateClassResponse,
    })
    @UseGuards(RoleGuard(UserRole.Admin))
    public async createClass(@Body() body: CreateClassRequest) {
        return this.classService.createClass(body);
    }

    @Post('/from-file')
    @ApiResponse({
        description: 'Создать классы из файла\n' +
            'Формат файла: CSV файл parallelId;teacherId;studyYearId;letter',
        status: 200,
        type: CreateClassesFromFileResponse,
    })
    @UseInterceptors(FileInterceptor('file'))
    @UseGuards(RoleGuard(UserRole.Admin))
    public async createClassesFromFile(
        @UploadedFile(FileValidationPipe)
            file: Express.Multer.File
    ) {
        return this.classService.createClassesFromFile(file);
    }

    @Put(':id')
    @ApiResponse({
		description: 'Обновить класс по id',
		status: 200,
		type: UpdateClassResponse,
    })
    @UseGuards(RoleGuard(UserRole.Admin))
    public async updateClass(@Param() param: any, @Body() body: UpdateClassRequest) {
        const request = new UpdateClassRequest(param.id, body.teacherId, body.parallelId, body.studyYearId);
        return this.classService.updateClass(request);
    }

    @Delete(':id')
    @ApiResponse({
        description: 'Удалить класс по id',
        status: 200,
        type: DeleteClassResponse,
    })
    @UseGuards(RoleGuard(UserRole.Admin))
    public async deleteTeacher(@Param() param: any) {
        const request = new DeleteClassRequest(param.id);
        return this.classService.deleteClass(request);
    }
}