import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {ClassEntity} from './class.entity';
import {CustomException} from "../exceptions/custom.exception";
import {GetClassDto} from "./dto/getClass.dto";
import GetClassRequest = GetClassDto.GetClassRequest;
import GetClassResponse = GetClassDto.GetClassResponse;
import GetClassesResponse = GetClassDto.GetClassesResponse;
import {CreateClassDto} from "./dto/createClassDto";
import CreateClassRequest = CreateClassDto.CreateClassRequest;
import CreateClassResponse = CreateClassDto.CreateClassResponse;
import {UpdateClassDto} from "./dto/updateClass.dto";
import UpdateClassRequest = UpdateClassDto.UpdateClassRequest;
import UpdateClassResponse = UpdateClassDto.UpdateClassResponse;
import {DeleteClassDto} from "./dto/deleteClass.dto";
import DeleteClassRequest = DeleteClassDto.DeleteClassRequest;
import DeleteClassResponse = DeleteClassDto.DeleteClassResponse;
import TeacherService from "../teacher/teacher.service";
import ParallelService from "../parallel/parallel.service";
import StudyYearService from "../studyYear/studyYear.service";
import {GetTeachersDto} from "../teacher/dto/getTeacher.dto";
import {GetParallelDto} from "../parallel/dto/getParallel.dto";
import {GetYearDTO} from "../studyYear/dto/getYear.dto";
import { Readable } from "stream";
import { CreateClassesFromFileDto } from "./dto/createClassesFromFile.dto";
import { CsvParser } from "nest-csv-parser";
import { NameChecker } from "../utils/nameChecker";
import CreateClassesFromFileResponse = CreateClassesFromFileDto.CreateClassesFromFileResponse;

@Injectable()
export default class ClassService {
    constructor(
        @InjectRepository(ClassEntity)
        private readonly classesRepository: Repository<ClassEntity>,
        private readonly teacherService: TeacherService,
        private readonly parallelService: ParallelService,
        private readonly studyYearService: StudyYearService,
        private readonly csvParser: CsvParser
    ) { }

    public async getClass(request: GetClassRequest): Promise<GetClassResponse> {
        const classEntity = await this.classesRepository.findOne({
            where: {
                id: request.classId
            }
        });
        
        if (!classEntity)
            throw CustomException.invalidData(`Класс с id ${request.classId} не найден`);
        
        return new GetClassResponse(classEntity);
    }

    public async getClasses(): Promise<GetClassesResponse> {
        const classes = await this.classesRepository.find();
        return new GetClassesResponse(classes);
    }
    
    public async getClassByParallelAndLetter(parallelId: number, letter: string): Promise<ClassEntity | undefined> {
        return await this.classesRepository.findOne({
            where: {
                parallelId: parallelId,
                letter: letter
            }
        });
    }

    public async createClass(request: CreateClassRequest): Promise<CreateClassResponse> {
        await this.teacherService.getTeacher(new GetTeachersDto.GetTeacherRequest(request.teacherId));
        await this.parallelService.getParallel(new GetParallelDto.GetParallelRequest(request.parallelId));
        await this.studyYearService.getStudyYear(new GetYearDTO.GetYearByIdRequest(request.studyYearId));

        await NameChecker.check(request.letter, 'classLetter');
        
        if (!!(await this.getClassByParallelAndLetter(request.parallelId, request.letter)))
            throw CustomException.invalidData(`Класс с параллелью ${request.parallelId}}`
                + `и буквой ${request.letter} уже существует`);
        
        const createdClass = await this.classesRepository.create({
            teacherId: request.teacherId,
            parallelId: request.parallelId,
            studyYearId: request.studyYearId,
            letter: request.letter
        });
        
        const savedClass = await this.classesRepository.save(createdClass);
        if (!savedClass)
          throw CustomException.internalError('Error upon creating class');
        
        return new CreateClassResponse(savedClass);
    }

    public async createClassesFromFile(file: Express.Multer.File): Promise<CreateClassesFromFileResponse> {
        try {
            //Формат файла: parallelId;teacherId;studyYearId;letter
            const stream = Readable.from(file.buffer);
            const classes = <ClassEntity[]>(await this.csvParser.parse(stream, ClassEntity)).list;

            const result: ClassEntity[] = [];
            let errorMessages = '';

            for (const classEntity of classes) {
                try {
                    await this.teacherService.getTeacher(new GetTeachersDto.GetTeacherRequest(classEntity.teacherId));
                    await this.parallelService.getParallel(new GetParallelDto.GetParallelRequest(classEntity.parallelId));
                    await this.studyYearService.getStudyYear(new GetYearDTO.GetYearByIdRequest(classEntity.studyYearId));
                    await NameChecker.check(classEntity.letter, 'classLetter');
                    
                    result.push(
                        (await this.createClass(new CreateClassRequest(
                            classEntity.parallelId,
                            classEntity.teacherId,
                            classEntity.studyYearId,
                            classEntity.letter
                        ))).classEntity
                    )
                }
                catch (error: any) {
                    if (error instanceof CustomException)
                        errorMessages += `${error.message}\n`
                    else
                        errorMessages += `${JSON.stringify(error.message, null, 4)}\n`
                }
            }

            return new CreateClassesFromFileResponse(result, errorMessages);
        }
        catch (e: any) {
            console.error(JSON.stringify(e, null, 4));
            throw CustomException.internalError(e)
        }
    }
    
    public async updateClass(request: UpdateClassRequest): Promise<UpdateClassResponse> {
        let {classEntity} = await this.getClass(new GetClassRequest(request.classId));
        
        if (request.teacherId) {
            await this.teacherService.getTeacher(new GetTeachersDto.GetTeacherRequest(request.teacherId));
            classEntity.teacherId = request.teacherId;
        }
        if (request.parallelId) {
            await this.parallelService.getParallel(new GetParallelDto.GetParallelRequest(request.parallelId));
            const newClassRequest = new CreateClassRequest(
                request.parallelId,
                classEntity.teacherId,
                classEntity.studyYearId,
                classEntity.letter
            );
            
            await this.classesRepository.delete(classEntity.id);

            classEntity = (await this.createClass(newClassRequest)).classEntity;
        }
        if (request.studyYearId) {
            await this.studyYearService.getStudyYear(new GetYearDTO.GetYearByIdRequest(request.studyYearId));
            classEntity.studyYearId = request.studyYearId;
        }
        if (request.letter) {
            await NameChecker.check(request.letter, 'classLetter');
            classEntity.letter = request.letter;
        }
        
        const savedClass = await this.classesRepository.save(classEntity);
        if (!savedClass)
            throw CustomException.internalError('Error upon updating class');
        
        return new UpdateClassResponse(savedClass);

    }

    public async deleteClass(request: DeleteClassRequest): Promise<DeleteClassResponse> {
        await this.classesRepository.delete(request.classId);
        return new DeleteClassResponse(true);
    }
}