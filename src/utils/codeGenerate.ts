﻿export class CodeGenerate {
    static async generate(length: number, isTest: boolean): Promise<string> {
        const code = Array.apply(null, Array(length)).map(function () {return 0});
        
        if (isTest)
            return code.join('');

        for (let i = 0; i < code.length; i++) {
            code[i] = Math.floor(Math.random() * 10);
        }
        
        return code.join('');
    }
}