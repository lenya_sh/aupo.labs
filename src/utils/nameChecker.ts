﻿import { CustomException } from "../exceptions/custom.exception";

export class NameChecker {
	static async check(value: string, typeOfCheck: 'names' | 'classLetter'): Promise<boolean> {
		const templateRegex = typeOfCheck == 'names' 
			? new RegExp("^[a-яА-Я]{2,50}$")
			: new RegExp('^[а-яА-я]{1,2}$');

		if (!templateRegex.test(value))
			throw CustomException.invalidData(`Значение ${value} некорректно`);

		return true;
	}
}