﻿export class Converters {
    static async pieceToCbm(quantityInPiece: number, coefficient: number): Promise<number> {
        return Number((quantityInPiece / coefficient).toFixed(3));
    }

    static async cbmToPiece(quantityInCbm: number, coefficient: number): Promise<number> {
        const result = quantityInCbm * coefficient;
        return Math.floor(result) + 0.9 <= result
            ? Math.round(result)
            : Math.floor(result);
    }
}