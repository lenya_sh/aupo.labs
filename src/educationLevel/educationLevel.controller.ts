import {Body, Controller, Delete, Get, Param, Post, Put, UseGuards} from "@nestjs/common";
import { ApiResponse, ApiTags } from "@nestjs/swagger";
import EducationLevelService from "./educationLevel.service";
import {GetEducationLevelDto} from "./dto/getEducationLevel.dto";
import GetEducationLevelsResponse = GetEducationLevelDto.GetEducationLevelsResponse;
import GetEducationLevelResponse = GetEducationLevelDto.GetEducationLevelResponse;
import GetEducationLevelRequest = GetEducationLevelDto.GetEducationLevelRequest;
import {CreateEducationLevelDto} from "./dto/createEducationLevel.dto";
import CreateEducationLevelRequest = CreateEducationLevelDto.CreateEducationLevelRequest;
import CreateEducationLevelResponse = CreateEducationLevelDto.CreateEducationLevelResponse;
import {JwtAuthenticationGuard} from "../authentication/jwt-authentication.guard";
import {UpdateEducationLevelDto} from "./dto/updateEducationLevel.dto";
import UpdateEducationLevelRequest = UpdateEducationLevelDto.UpdateEducationLevelRequest;
import UpdateEducationLevelResponse = UpdateEducationLevelDto.UpdateEducationLevelResponse;
import {DeleteEducationLevelDto} from "./dto/deleteEducationLevel.dto";
import DeleteEducationLevelResponse = DeleteEducationLevelDto.DeleteEducationLevelResponse;
import DeleteEducationLevelRequest = DeleteEducationLevelDto.DeleteEducationLevelRequest;

@Controller('education-levels')
@ApiTags('Учебные грейды')
export class EducationLevelController {
    constructor(
        private educationLevelService: EducationLevelService
    ) { }
    
    @Get()
    @ApiResponse({
		description: 'Получить все учебные грейды',
		status: 200,
		type: GetEducationLevelsResponse,
	})
    @UseGuards(JwtAuthenticationGuard)
    public async getEducationLevels() {
        return this.educationLevelService.getEducationLevels();
    }

    @Get(':id')
    @ApiResponse({
        description: 'Получить учебный грейд по id',
        status: 200,
        type: GetEducationLevelResponse,
    })
    @UseGuards(JwtAuthenticationGuard)
    public async getEducationLevel(@Param() param: any) {
        const request = new GetEducationLevelRequest(param.id);
        return this.educationLevelService.getEducationLevel(request);
    }

    @Post()
    @ApiResponse({
        description: 'Создать учебный грейд',
        status: 200,
        type: CreateEducationLevelResponse,
    })
    @UseGuards(JwtAuthenticationGuard)
    public async createEducationLevel(@Body() body: CreateEducationLevelRequest) {
        return this.educationLevelService.createEducationLevel(body);
    }

    @Put(':id')
    @ApiResponse({
		description: 'Обновить учебный грейд по id',
		status: 200,
		type: UpdateEducationLevelResponse,
    })
    @UseGuards(JwtAuthenticationGuard)
    public async updateEducationLevel(@Param() param: any, @Body() body: UpdateEducationLevelRequest) {
        const request = new UpdateEducationLevelRequest(param.id, body.educationLevel);
        return this.educationLevelService.updateEducationLevel(request);
    }

    @Delete(':id')
    @ApiResponse({
        description: 'Удалить учебный грейд по id',
        status: 200,
        type: DeleteEducationLevelResponse,
    })
    @UseGuards(JwtAuthenticationGuard)
    public async deleteEducationLevel(@Param() param: any) {
        const request = new DeleteEducationLevelRequest(param.id);
        return this.educationLevelService.deleteEducationLevel(request);
    }
}