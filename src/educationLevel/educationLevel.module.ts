import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EducationLevelController } from './educationLevel.controller';
import { EducationLevelEntity } from './educationLevel.entity';
import EducationLevelService from './educationLevel.service';

@Module({
  imports: [TypeOrmModule.forFeature([EducationLevelEntity])],
  exports: [EducationLevelService],
  providers: [EducationLevelService],
  controllers: [EducationLevelController]
})
export class EducationLevelModule {}
