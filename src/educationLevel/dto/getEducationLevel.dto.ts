﻿import { ApiProperty } from "@nestjs/swagger";
import {IsDefined, IsInt} from "class-validator";
import { EducationLevelEntity } from "../educationLevel.entity";

export namespace GetEducationLevelDto {
    export class GetEducationLevelRequest {
        @IsDefined({
            message: 'Education level id is defined'
        })
        @IsInt({
            message: 'Education level id must be an int'
        })
        @ApiProperty()
        public educationLevelId: number;

        constructor(educationLevelId: number) {
            this.educationLevelId = educationLevelId;
        }
    }
    
    export class GetEducationLevelsResponse {
        @ApiProperty()
        public educationLevels: EducationLevelEntity[];
        
        constructor(educationLevels: EducationLevelEntity[]) {
            this.educationLevels = educationLevels;
        }
    }

    export class GetEducationLevelResponse {
        @ApiProperty()
        public educationLevel: EducationLevelEntity;

        constructor(educationLevel: EducationLevelEntity) {
            this.educationLevel = educationLevel;
        }
    }
}