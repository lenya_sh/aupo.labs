﻿import { ApiProperty } from "@nestjs/swagger";
import { IsDefined, IsIn, IsString} from "class-validator";
import { Constants, EducationLevel } from "../../constants";
import { EducationLevelEntity } from "../educationLevel.entity";

export namespace CreateEducationLevelDto {
  export class CreateEducationLevelRequest {
    @IsDefined({
      message: 'Education level is defined'
    })
    @IsString({
      message: 'Education level must be a string'
    })
    @IsIn(Constants.EducationLevel)
    @ApiProperty()
    public educationLevel: EducationLevel;

    constructor(educationLevel: EducationLevel) {
      this.educationLevel = educationLevel;
    }
  }

  export class CreateEducationLevelResponse {
    @ApiProperty()
    public educationLevel: EducationLevelEntity;

    constructor(educationLevel: EducationLevelEntity) {
      this.educationLevel = educationLevel;
    }
  }
}