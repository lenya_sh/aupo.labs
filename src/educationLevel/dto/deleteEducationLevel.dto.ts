﻿import { ApiProperty } from "@nestjs/swagger";
import {IsDefined} from "class-validator";

export namespace DeleteEducationLevelDto {
    export class DeleteEducationLevelRequest {
        @IsDefined ({ message: 'Level id is required'})
        @ApiProperty()
        public levelId: number;

        constructor(levelId: number) {
            this.levelId = levelId;
        }
    }

    export class DeleteEducationLevelResponse {
        @ApiProperty()
        public status: boolean;

        constructor(status: boolean) {
            this.status = status;
        }
    }
}