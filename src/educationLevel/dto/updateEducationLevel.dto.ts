﻿import { ApiProperty } from "@nestjs/swagger";
import {IsDefined, IsIn, IsOptional, IsString} from "class-validator";
import { Constants, EducationLevel } from "../../constants";
import { EducationLevelEntity } from "../educationLevel.entity";

export namespace UpdateEducationLevelDto {
    export class UpdateEducationLevelRequest {
        public levelId: number;
        
        @IsDefined({
            message: 'Education level is defined'
        })
        @IsString({
            message: 'Education level must be a string'
        })
        @IsIn(Constants.EducationLevel)
        @ApiProperty()
        public educationLevel: EducationLevel;

        constructor(levelId: number, educationLevel: EducationLevel) {
            this.levelId = levelId;
            this.educationLevel = educationLevel;
        }
    }

    export class UpdateEducationLevelResponse {
        @ApiProperty()
        public educationLevel: EducationLevelEntity;

        constructor(educationLevel: EducationLevelEntity) {
            this.educationLevel = educationLevel;
        }
    }
}