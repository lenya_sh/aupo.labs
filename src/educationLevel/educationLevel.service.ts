import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {EducationLevelEntity} from './educationLevel.entity';
import {CreateEducationLevelDto} from "./dto/createEducationLevel.dto";
import CreateEducationLevelRequest = CreateEducationLevelDto.CreateEducationLevelRequest;
import CreateEducationLevelResponse = CreateEducationLevelDto.CreateEducationLevelResponse;
import {CustomException} from "../exceptions/custom.exception";
import {GetEducationLevelDto} from "./dto/getEducationLevel.dto";
import GetEducationLevelsResponse = GetEducationLevelDto.GetEducationLevelsResponse;
import GetEducationLevelRequest = GetEducationLevelDto.GetEducationLevelRequest;
import GetEducationLevelResponse = GetEducationLevelDto.GetEducationLevelResponse;
import {UpdateEducationLevelDto} from "./dto/updateEducationLevel.dto";
import UpdateEducationLevelRequest = UpdateEducationLevelDto.UpdateEducationLevelRequest;
import UpdateEducationLevelResponse = UpdateEducationLevelDto.UpdateEducationLevelResponse;
import {DeleteEducationLevelDto} from "./dto/deleteEducationLevel.dto";
import DeleteEducationLevelRequest = DeleteEducationLevelDto.DeleteEducationLevelRequest;
import DeleteEducationLevelResponse = DeleteEducationLevelDto.DeleteEducationLevelResponse;
import {DeleteTeacherDto} from "../teacher/dto/deleteTeacher.dto";
import DeleteTeacherResponse = DeleteTeacherDto.DeleteTeacherResponse;

@Injectable()
export default class EducationLevelService {
    constructor(
        @InjectRepository(EducationLevelEntity)
        private readonly educationLevelRepository: Repository<EducationLevelEntity>
    ) { }

    public async getEducationLevel(request: GetEducationLevelRequest): Promise<GetEducationLevelResponse> {
        const level = await this.educationLevelRepository.findOne({
            where: {
                id: request.educationLevelId
            }
        });
        
        if (!level)
            throw CustomException.invalidData(`Грейд с id ${request.educationLevelId} не найден`);
        
        return new GetEducationLevelResponse(level);
    }

    public async getEducationLevels(): Promise<GetEducationLevelsResponse> {
        const levels = await this.educationLevelRepository.find();
        return new GetEducationLevelsResponse(levels);
    }

    public async createEducationLevel(request: CreateEducationLevelRequest): Promise<CreateEducationLevelResponse> {
        const createdLevel = await this.educationLevelRepository.create({
            educationLevel: request.educationLevel
        });
        
        const savedLevel = await this.educationLevelRepository.save(createdLevel);
        if (!savedLevel)
          throw CustomException.internalError('Error upon creating education level');
        
        return new CreateEducationLevelDto.CreateEducationLevelResponse(savedLevel);
    }

    public async updateEducationLevel(request: UpdateEducationLevelRequest): Promise<UpdateEducationLevelResponse> {
        const {educationLevel: level} = await this.getEducationLevel(new GetEducationLevelRequest(request.levelId!));
        
        level.educationLevel = request.educationLevel;
        
        const savedLevel = await this.educationLevelRepository.save(level);
        if (!savedLevel)
            throw CustomException.internalError('Error upon updating education level');
        
        return new UpdateEducationLevelResponse(savedLevel);

    }

    public async deleteEducationLevel(request: DeleteEducationLevelRequest): Promise<DeleteTeacherResponse> {
        await this.educationLevelRepository.delete(request.levelId);
        return new DeleteEducationLevelResponse(true);
    }
}