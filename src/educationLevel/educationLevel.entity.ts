import {ApiProperty} from '@nestjs/swagger';
import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { EducationLevel } from "../constants";
import { ParallelEntity } from "../parallel/parallel.entity";

@Entity({ name: 'education_level' })
export class EducationLevelEntity {
	@PrimaryGeneratedColumn()
	@ApiProperty()
	id!: number;

	@ApiProperty()
	@Column('varchar', { name: 'education_level', unique: true })
	educationLevel!: EducationLevel;

	@OneToMany(() => ParallelEntity, parallel => parallel.educationLevelId)
	@ApiProperty({ type: () => ParallelEntity})
	public parallels!: ParallelEntity[];

	@ApiProperty()
	@CreateDateColumn({ type: 'timestamp', name: 'created_at' })
	readonly createdAt!: Date;
  
	@ApiProperty()
	@UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
	readonly updatedAt!: Date;
}