import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { CustomValidationPipe } from './pipes/validation.pipe';
import express from "express";
import { CustomExceptionFilter } from "./exceptions/CustomException.filter";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.use(express.json({limit:'8mb'}));
  app.useGlobalPipes(new CustomValidationPipe());
  app.useGlobalFilters(new CustomExceptionFilter());
  //app.use(cookieParser());
  // app.enableCors({
  //   origin: configService.get('FRONTEND_URL'),
  //   credentials: true,
  // });

  const config = new DocumentBuilder()
    .setTitle('app')
    .setDescription('API для aupo')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('docs', app, document);

  await app.listen(3000);
}
bootstrap();