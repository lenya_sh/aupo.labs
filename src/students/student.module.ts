import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StudentController } from './student.controller';
import { StudentEntity } from './student.entity';
import StudentService from './student.service';
import { ClassModule } from "../class/class.module";
import { CsvModule } from "nest-csv-parser";

@Module({
  imports: [
    TypeOrmModule.forFeature([StudentEntity]),
    ClassModule,
    CsvModule,
  ],
  exports: [StudentService],
  providers: [StudentService],
  controllers: [StudentController]
})
export class StudentModule {}
