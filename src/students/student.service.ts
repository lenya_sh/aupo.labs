import { Injectable } from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {StudentEntity} from "./student.entity";
import {Repository} from "typeorm";
import {DeactivateStudentDto} from "./dto/deleteStudent.dto";
import {GetStudentDto} from "./dto/getStudent.dto";
import GetStudentsResponse = GetStudentDto.GetStudentsResponse;
import {CreateStudentDto} from "./dto/createStudentDto";
import CreateStudentResponse = CreateStudentDto.CreateStudentResponse;
import {UpdateStudentDto} from "./dto/updateStudent.dto";
import UpdateStudentRequest = UpdateStudentDto.UpdateStudentRequest;
import {CustomException} from "../exceptions/custom.exception";
import GetStudentRequest = GetStudentDto.GetStudentRequest;
import CreateStudentRequest = CreateStudentDto.CreateStudentRequest;
import GetStudentResponse = GetStudentDto.GetStudentResponse;
import UpdateStudentResponse = UpdateStudentDto.UpdateStudentResponse;
import DeactivateStudentRequest = DeactivateStudentDto.DeactivateStudentRequest;
import DeactivateStudentResponse = DeactivateStudentDto.DeactivateStudentResponse;
import ClassService from "../class/class.service";
import { GetClassDto } from "../class/dto/getClass.dto";
import GetClassRequest = GetClassDto.GetClassRequest;
import { NameChecker } from "../utils/nameChecker";
import { CsvParser } from "nest-csv-parser";
import { Readable } from "stream";
import { CreateStudentsFromFileDto } from "./dto/createStudentsFromFile.dto";
import CreateStudentsFromFileResponse = CreateStudentsFromFileDto.CreateStudentsFromFileResponse;

@Injectable()
export default class StudentService {
    constructor(
        @InjectRepository(StudentEntity)
        private readonly studentRepository: Repository<StudentEntity>,
        private readonly classService: ClassService,
        private readonly csvParser: CsvParser
    ) { }

    public async getStudent(request: GetStudentRequest): Promise<GetStudentResponse> {
        const student = await this.studentRepository.findOne({
            where: {
                id: request.studentId
            }
        });
        
        if (!student)
            throw CustomException.invalidData(`Ученик с id ${request.studentId} не найден`);
        
        return new GetStudentResponse(student);
    }

    public async getActiveStudents(): Promise<GetStudentsResponse> {
        const students = await this.studentRepository.find({
            where: {
                active: true
            }
        });
        
        return new GetStudentsResponse(students);
    }

    public async getAllStudents(): Promise<GetStudentsResponse> {
        const students = await this.studentRepository.find();
        return new GetStudentsResponse(students);
    }

    public async createStudent(request: CreateStudentRequest): Promise<CreateStudentResponse> {
        await this.classService.getClass(new GetClassRequest(request.classId));
        await NameChecker.check(request.firstName, 'names');
        await NameChecker.check(request.lastName, 'names');
        if (request.patronymic)
            await NameChecker.check(request.patronymic, 'names');
        
        const createdStudent = await this.studentRepository.create({
            firstName: request.firstName,
            lastName: request.lastName,
            classId: request.classId,
            patronymic: request.patronymic
        });
        
        const savedStudent = await this.studentRepository.save(createdStudent);
        if (!savedStudent)
          throw CustomException.internalError('Error upon creating student');
        
        return new CreateStudentResponse(savedStudent);
    }

    public async createStudentsFromFile(file: Express.Multer.File): Promise<CreateStudentsFromFileResponse> {
        try {
            //Формат файла: lastName;firstName;patronymic;classId
            const stream = Readable.from(file.buffer);
            const students = <StudentEntity[]>(await this.csvParser.parse(stream, StudentEntity)).list;
            
            const result: StudentEntity[] = [];
            let errorMessages = '';

            for (const student of students) {
                try {
                    await this.classService.getClass(new GetClassRequest(student.classId));
                    await NameChecker.check(student.firstName, 'names');
                    await NameChecker.check(student.lastName, 'names');
                    if (student.patronymic)
                        await NameChecker.check(student.patronymic, 'names');
                    
                    result.push(
                        (await this.createStudent(new CreateStudentDto.CreateStudentRequest(
                            student.firstName,
                            student.lastName,
                            Number(student.classId),
                            student.patronymic == '' ? undefined : student.patronymic
                        ))).student
                    )
                }
                catch (error: any) {
                    if (error instanceof CustomException)
                        errorMessages += `${error.message}\n`
                    else
                        errorMessages += `${JSON.stringify(error.message, null, 4)}\n`
                }
            }
            
            return new CreateStudentsFromFileResponse(result, errorMessages);
        }
        catch (e: any) {
            console.error(JSON.stringify(e, null, 4));
            throw CustomException.internalError(e)
        }
    }
    
    public async updateStudent(request: UpdateStudentRequest): Promise<UpdateStudentResponse> {
        let {student} = await this.getStudent(new GetStudentRequest(request.studentId));
        
        if (request.firstName) {
            await NameChecker.check(request.firstName, 'names');
            student.firstName = request.firstName;
        }
        if (request.lastName) {
            await NameChecker.check(request.lastName, 'names');
            student.lastName = request.lastName;
        }
        if (request.patronymic) {
            await NameChecker.check(request.patronymic, 'names');
            student.patronymic = request.patronymic;
        }
        if (request.classId) {
            await this.classService.getClass(new GetClassRequest(request.classId));
            
            student.active = false;
            const savedStudent = await this.studentRepository.save(student);
            if (!savedStudent)
                throw CustomException.internalError('Error upon updating student');
            
            const {student: newStudent} = await this.createStudent(new CreateStudentDto.CreateStudentRequest(
              student.firstName,
              student.lastName,
              request.classId,
              student.patronymic
            ));
            
            return new UpdateStudentResponse(newStudent);
        }
        
        const savedStudent = await this.studentRepository.save(student);
        if (!savedStudent)
            throw CustomException.internalError('Error upon updating student');
        
        return new UpdateStudentResponse(savedStudent);
    }

    public async deactivateStudent(request: DeactivateStudentRequest): Promise<DeactivateStudentResponse> {
        const {student} = await this.getStudent(new GetStudentRequest(request.studentId));
        
        student.active = false;

        const savedStudent = await this.studentRepository.save(student);
        if (!savedStudent)
            throw CustomException.internalError('Error upon updating student');
        
        return new DeactivateStudentResponse(true);
    }
}