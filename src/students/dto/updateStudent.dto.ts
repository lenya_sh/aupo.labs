﻿import {ApiProperty, ApiPropertyOptional} from "@nestjs/swagger";
import {IsInt, IsOptional, IsString} from "class-validator";
import { StudentEntity } from "../student.entity";

export namespace UpdateStudentDto {
    export class UpdateStudentRequest {
        public studentId: number;
        
        @IsOptional({
            message: 'First name is optional'
        })
        @IsString({
            message: 'First name must be a string'
        })
        @ApiPropertyOptional({example: "Формат: строка длиной от 2 до 50 символов"})
        public firstName?: string;

        @IsOptional({
            message: 'Last name is optional'
        })
        @IsString({
            message: 'Last name must be a string'
        })
        @ApiPropertyOptional({example: "Формат: строка длиной от 2 до 50 символов"})
        public lastName?: string;

        @IsOptional({
            message: 'Patronymic is optional'
        })
        @IsString({
            message: 'Patronymic must be a string'
        })
        @ApiPropertyOptional({example: "Формат: строка длиной от 2 до 50 символов"})
        public patronymic?: string;

        @IsOptional({
            message: 'Class id is optional'
        })
        @IsInt()
        @ApiPropertyOptional()
        public classId?: number;

        constructor(studentId: number, firstName?: string, lastName?: string, patronymic?: string, classId?: number) {
            this.studentId = studentId;
            this.firstName = firstName;
            this.lastName = lastName;
            this.patronymic = patronymic;
            this.classId = classId;
        }
    }

    export class UpdateStudentResponse {
        @ApiProperty()
        public student: StudentEntity;

        constructor(student: StudentEntity) {
            this.student = student;
        }
    }
}