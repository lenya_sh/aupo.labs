﻿import {ApiProperty, ApiPropertyOptional} from "@nestjs/swagger";
import {IsDefined, IsInt, IsOptional, IsString} from "class-validator";
import {StudentEntity} from "../student.entity";

export namespace CreateStudentDto {
    export class CreateStudentRequest {
        @IsDefined({
            message: 'First name is defined'
        })
        @IsString({
            message: 'First name must be a string'
        })
        @ApiProperty({example: "Формат: строка длиной от 2 до 50 символов"})
        public firstName: string;

        @IsDefined({
            message: 'Last name is defined'
        })
        @IsString({
            message: 'Last name must be a string'
        })
        @ApiProperty({example: "Формат: строка длиной от 2 до 50 символов"})
        public lastName: string;

        @IsOptional({
            message: 'Patronymic is defined'
        })
        @IsString({
            message: 'Patronymic must be a string'
        })
        @ApiPropertyOptional({example: "Формат: строка длиной от 2 до 50 символов"})
        public patronymic?: string;

        @IsDefined({
            message: 'Class id is defined'
        })
        @IsInt()
        @ApiProperty()
        public classId: number;

        constructor(firstName: string, lastName: string, classId: number, patronymic?: string) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.classId = classId;
            this.patronymic = patronymic;
        }
    }

    export class CreateStudentResponse {
        @ApiProperty()
        public student: StudentEntity;

        constructor(student: StudentEntity) {
            this.student = student;
        }
    }
}