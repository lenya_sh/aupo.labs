﻿import { ApiProperty } from "@nestjs/swagger";
import {IsDefined} from "class-validator";

export namespace DeactivateStudentDto {
    export class DeactivateStudentRequest {
        @IsDefined ({ message: 'Student id is required'})
        @ApiProperty()
        public studentId: number;

        constructor(studentId: number) {
            this.studentId = studentId;
        }
    }

    export class DeactivateStudentResponse {
        @ApiProperty()
        public status: boolean;

        constructor(status: boolean) {
            this.status = status;
        }
    }
}