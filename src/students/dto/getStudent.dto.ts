﻿import { ApiProperty } from "@nestjs/swagger";
import {IsDefined, IsInt} from "class-validator";
import { StudentEntity } from "../student.entity";

export namespace GetStudentDto {
    export class GetStudentRequest {
        @IsDefined({
            message: 'Student id is defined'
        })
        @IsInt({
            message: 'Student id must be an int'
        })
        @ApiProperty()
        public studentId: number;

        constructor(studentId: number) {
            this.studentId = studentId;
        }
    }
    
    export class GetStudentsResponse {
        @ApiProperty()
        public students: StudentEntity[];
        
        constructor(students: StudentEntity[]) {
            this.students = students;
        }
    }

    export class GetStudentResponse {
        @ApiProperty()
        public student: StudentEntity;

        constructor(student: StudentEntity) {
            this.student = student;
        }
    }
}