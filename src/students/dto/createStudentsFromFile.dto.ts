﻿import {ApiProperty} from "@nestjs/swagger";
import {StudentEntity} from "../student.entity";

export namespace CreateStudentsFromFileDto {
	export class CreateStudentsFromFileResponse {
		@ApiProperty()
		public students: StudentEntity[];
		
		@ApiProperty()
		public errorsMessages: string;

		constructor(students: StudentEntity[], errorMessages: string) {
			this.students = students;
			this.errorsMessages	= errorMessages;
		}
	}
}