import {ApiProperty} from '@nestjs/swagger';
import {
	Column,
	CreateDateColumn,
	Entity,
	JoinColumn,
	ManyToOne,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm';
import { EducationLevel } from "../constants";
import {StudyYearEntity} from "../studyYear/studyYear.entity";
import {ClassEntity} from "../class/class.entity";

@Entity({ name: 'students' })
export class StudentEntity {
	@ApiProperty()
	@PrimaryGeneratedColumn()
	public id!: number;

	@ApiProperty()
	@Column('varchar', { name: 'first_name' })
	firstName!: string;

	@ApiProperty()
	@Column('varchar', { name: 'last_name' })
	lastName!: string;

	@ApiProperty()
	@Column('varchar', { name: 'patronymic', nullable: true })
	patronymic!: string;

	@ApiProperty()
	@Column({ name: 'class_id', type: 'int' })
	public classId!: number;

	@ManyToOne(() => ClassEntity, classEntity => classEntity.students)
	@JoinColumn({ name: 'class_id' })
	@ApiProperty({ type: () => ClassEntity})
	public classEntity!: ClassEntity;

	@Column({ name: 'active', type: 'boolean', default: true})
	@ApiProperty()
	public active!: boolean;

	@ApiProperty()
	@CreateDateColumn({ type: 'timestamp', name: 'created_at' })
	readonly createdAt!: Date;
  
	@ApiProperty()
	@UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
	readonly updatedAt!: Date;
}