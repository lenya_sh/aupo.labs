import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put, UploadedFile,
    UseGuards, UseInterceptors
} from "@nestjs/common";
import {ApiResponse, ApiTags} from "@nestjs/swagger";
import StudentService from "./student.service";
import {JwtAuthenticationGuard} from "../authentication/jwt-authentication.guard";
import {RoleGuard} from "../authentication/role.guard";
import {UserRole} from "../constants";
import {GetStudentDto} from "./dto/getStudent.dto";
import {CreateStudentDto} from "./dto/createStudentDto";
import {UpdateStudentDto} from "./dto/updateStudent.dto";
import GetStudentsResponse = GetStudentDto.GetStudentsResponse;
import GetStudentRequest = GetStudentDto.GetStudentRequest;
import GetStudentResponse = GetStudentDto.GetStudentResponse;
import CreateStudentResponse = CreateStudentDto.CreateStudentResponse;
import CreateStudentRequest = CreateStudentDto.CreateStudentRequest;
import UpdateStudentResponse = UpdateStudentDto.UpdateStudentResponse;
import UpdateStudentRequest = UpdateStudentDto.UpdateStudentRequest;
import {DeactivateStudentDto} from "./dto/deleteStudent.dto";
import DeactivateStudentResponse = DeactivateStudentDto.DeactivateStudentResponse;
import DeactivateStudentRequest = DeactivateStudentDto.DeactivateStudentRequest;
import { FileInterceptor } from "@nestjs/platform-express";
import { FileValidationPipe } from "../pipes/file.validator";

@Controller('students')
@ApiTags('Ученики')
export class StudentController {
    constructor(private studentService: StudentService) { }
    
    @Get()
    @ApiResponse({
		description: 'Получить список всех учеников',
		status: 200,
		type: GetStudentsResponse,
	})
    @UseGuards(RoleGuard(UserRole.Admin))
    public async getStudents() {
        return this.studentService.getAllStudents();
    }

    @Get('/active')
    @ApiResponse({
        description: 'Получить список всех учеников',
        status: 200,
        type: GetStudentsResponse,
    })
    @UseGuards(JwtAuthenticationGuard)
    public async getActiveStudents() {
        return this.studentService.getActiveStudents();
    }

    @Get(':id')
    @ApiResponse({
        description: 'Получить ученика по id',
        status: 200,
        type: GetStudentResponse,
    })
    @UseGuards(JwtAuthenticationGuard)
    public async getStudent(@Param() param: any) {
        const request = new GetStudentRequest(param.id);
        return this.studentService.getStudent(request);
    }

    @Post()
    @ApiResponse({
        description: 'Создать ученика',
        status: 200,
        type: CreateStudentResponse,
    })
    @UseGuards(RoleGuard(UserRole.Admin))
    public async createStudent(
        @Body() body: CreateStudentRequest) {
        return this.studentService.createStudent(body);
    }
    
    @Post('/from-file')
    @ApiResponse({
        description: 'Создать учеников из файла\nФормат файла: CSV файл lastName;firstName;patronymic;classId',
        status: 200,
        type: CreateStudentResponse,
    })
    @UseInterceptors(FileInterceptor('file'))
    @UseGuards(RoleGuard(UserRole.Admin))
    public async createStudentsFromFile(
        @UploadedFile(FileValidationPipe) file: Express.Multer.File) {
        return this.studentService.createStudentsFromFile(file);
    }

    @Put(':id')
    @ApiResponse({
		description: 'Обновить ученика по id',
		status: 200,
		type: UpdateStudentResponse,
    })
    @UseGuards(RoleGuard(UserRole.Admin))
    public async updateStudent(@Param() param: any, @Body() body: UpdateStudentRequest) {
        const request = new UpdateStudentRequest(param.id, body.firstName, body.lastName, body.patronymic, body.classId);
        return this.studentService.updateStudent(request);
    }

    @Delete(':id')
    @ApiResponse({
        description: 'Деактивировать ученика по id',
        status: 200,
        type: DeactivateStudentResponse,
    })
    @UseGuards(RoleGuard(UserRole.Admin))
    public async deactivateStudent(@Param() param: any) {
        const request = new DeactivateStudentRequest(param.id);
        return this.studentService.deactivateStudent(request);
    }
}