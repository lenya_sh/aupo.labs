import { Body, Controller, Get, Param, Post, Put, UseGuards } from "@nestjs/common";
import { ApiResponse, ApiTags } from "@nestjs/swagger";
import { RoleGuard } from "../authentication/role.guard";
import { UserRole } from "../constants";
import StudyYearService from "./studyYear.service";
import { GetYearDTO } from "./dto/getYear.dto";
import GetYearResponse = GetYearDTO.GetYearResponse;
import {UpdateYearDTO} from "./dto/updateYear.dto";
import UpdateYearRequest = UpdateYearDTO.UpdateYearRequest;
import UpdateYearResponse = UpdateYearDTO.UpdateYearResponse;
import { CreateStudyYearDto } from "./dto/createStudyYear.dto";
import CreateStudyYearResponse = CreateStudyYearDto.CreateStudyYearResponse;
import CreateStudyYearRequest = CreateStudyYearDto.CreateStudyYearRequest;
import GetYearsResponse = GetYearDTO.GetYearsResponse;
import GetYearByIdRequest = GetYearDTO.GetYearByIdRequest;

@Controller('study-years')
@ApiTags('Учебный год')
export class StudyYearController {
    constructor(
        private studyYearService: StudyYearService
    ) {}
	
	@Post()
	@ApiResponse({
		description: 'Создать учебный год',
		status: 200,
		type: CreateStudyYearResponse,
	})
	public async createStudyYear(@Body() body: CreateStudyYearRequest) {
		return this.studyYearService.createStudyYear(body);
	}
    
    @Get()
    @ApiResponse({
		description: 'Получить все учебные года',
		status: 200,
		type: GetYearsResponse,
	})
    public async getStudyYears() {
        return this.studyYearService.getStudyYears();
    }

	@Get(':id')
	@ApiResponse({
		description: 'Получить учебный год по id',
		status: 200,
		type: GetYearResponse,
	})

	public async getStudyYear(@Param() param: any) {
		return this.studyYearService.getStudyYear(new GetYearByIdRequest(param.id));
	}

    @Put(':id')
    @ApiResponse({
		description: 'Обновить значение текущего учебного года',
		status: 200,
		type: UpdateYearResponse,
    })
    @UseGuards(RoleGuard(UserRole.Admin))
    public async updateStudyYear(@Body() body: UpdateYearRequest, @Param() param: any) {
        return this.studyYearService.updateStudyYear(body);
    }
}