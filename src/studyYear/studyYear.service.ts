import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {GetYearDTO} from './dto/getYear.dto';
import {UpdateYearDTO} from './dto/updateYear.dto';
import {StudyYearEntity} from './studyYear.entity';
import { CustomException } from "../exceptions/custom.exception";
import UpdateYearRequest = UpdateYearDTO.UpdateYearRequest;
import UpdateYearResponse = UpdateYearDTO.UpdateYearResponse;
import GetYearByIdRequest = GetYearDTO.GetYearByIdRequest;
import { CreateStudyYearDto } from "./dto/createStudyYear.dto";
import CreateStudyYearRequest = CreateStudyYearDto.CreateStudyYearRequest;
import CreateStudyYearResponse = CreateStudyYearDto.CreateStudyYearResponse;
import GetYearsResponse = GetYearDTO.GetYearsResponse;
import GetYearResponse = GetYearDTO.GetYearResponse;

@Injectable()
export default class StudyYearService {
	constructor(
		@InjectRepository(StudyYearEntity)
		private readonly studyYearRepository: Repository<StudyYearEntity>,
	) { }
	
	public async createStudyYear(request: CreateStudyYearRequest): Promise<CreateStudyYearResponse> {
		await this.checkValueOfStudyYear(request.studyYear);
		
		const createdStudyYear = await this.studyYearRepository.create({
			value: request.studyYear
		});
		
		const savedYear = await this.studyYearRepository.save(createdStudyYear);
		if (!savedYear)
			throw CustomException.internalError('Error upon creating study year');
		
		return new CreateStudyYearResponse(savedYear);
	}

	public async getStudyYears(): Promise<GetYearsResponse> {
		let studyYears = await this.studyYearRepository.find();
		return new GetYearsResponse(studyYears);
	}

	public async getStudyYear(request: GetYearByIdRequest): Promise<GetYearResponse> {
		let studyYear = await this.studyYearRepository.findOne(request.studyYearId);

		if (!studyYear)
			throw CustomException.invalidData(`Учебный год с id ${request.studyYearId} не найден`);

		return new GetYearResponse(studyYear);
	}

	public async updateStudyYear(request: UpdateYearRequest): Promise<UpdateYearResponse> {
		const { studyYear } = await this.getStudyYear(new GetYearByIdRequest(request.studyYearId));

		studyYear.value = request.studyYearValue;

		const savedYear = await this.studyYearRepository.save(studyYear);
		if (!savedYear) {
			throw CustomException.internalError('Error upon updating study year');
		}

		return new UpdateYearResponse(savedYear);
	}
	
	public async checkValueOfStudyYear(value: string) {
		const firstAndSecondYear: string[] = value.replace(/[^0-9]+[^0-9]+/g, "")
			.split('/');
		const firstYear: number = +firstAndSecondYear[0];
		const secondYear: number = +firstAndSecondYear[1];
		
		if(!firstYear || !secondYear || secondYear - firstYear != 1)
			throw CustomException.invalidData(`Значение ${value} некорректно. Корректный формат 00/01`);
	}
}