import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {StudyYearController} from './studyYear.controller';
import { StudyYearEntity } from './studyYear.entity';
import StudyYearService from './studyYear.service';

@Module({
  imports: [TypeOrmModule.forFeature([StudyYearEntity])],
  exports: [StudyYearService],
  providers: [StudyYearService],
  controllers: [StudyYearController]
})
export class StudyYearModule {}
