import {ApiProperty} from "@nestjs/swagger";
import {Type} from "class-transformer";
import {IsDefined, IsString} from "class-validator";
import {StudyYearEntity} from "../studyYear.entity";

export namespace UpdateYearDTO {
    export class UpdateYearRequest {
        studyYearId: number;
        
        @IsDefined({
            message: 'Study year is defined'
        })
        @Type(() => String)
        @IsString()
        @ApiProperty({example: "Формат: 00/01"})
        studyYearValue: string;
        
        constructor(studyYearId: number, studyYearValue: string) {
            this.studyYearId = studyYearId;
            this.studyYearValue = studyYearValue;
        }
    }

    export class UpdateYearResponse {
        @ApiProperty()
        studyYear: StudyYearEntity;
        
        constructor(studyYear: StudyYearEntity) {
            this.studyYear = studyYear;
        }
    }
}