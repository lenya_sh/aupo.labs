﻿import { ApiProperty } from "@nestjs/swagger";
import { StudyYearEntity } from "../studyYear.entity";
import { IsString } from "class-validator";

export namespace CreateStudyYearDto {
	export class CreateStudyYearRequest {
		@ApiProperty({example: "Формат: 00/01"})
		@IsString()
		studyYear: string;

		constructor(studyYear: string) {
			this.studyYear = studyYear;
		}
	}

	export class CreateStudyYearResponse {
		@ApiProperty()
		studyYear: StudyYearEntity;

		constructor(studyYear: StudyYearEntity) {
			this.studyYear = studyYear;
		}
	}
}