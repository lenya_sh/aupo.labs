import { ApiProperty } from "@nestjs/swagger";
import { StudyYearEntity } from "../studyYear.entity";
import {IsNumber} from "class-validator";

export namespace GetYearDTO {
    export class GetYearByIdRequest {
        @ApiProperty()
        @IsNumber()
        studyYearId: number;

        constructor(studyYearId: number) {
            this.studyYearId = studyYearId;
        }
    }
    
    export class GetYearResponse {
        @ApiProperty()
        studyYear: StudyYearEntity;
        
        constructor(studyYear: StudyYearEntity) {
            this.studyYear = studyYear;
        }
    }

    export class GetYearsResponse {
        @ApiProperty()
        studyYears: StudyYearEntity[];

        constructor(studyYears: StudyYearEntity[]) {
            this.studyYears = studyYears;
        }
    }
}