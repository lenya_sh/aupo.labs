import {ApiProperty} from '@nestjs/swagger';
import {Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn,} from 'typeorm';
import moment from "moment";
import {ClassEntity} from "../class/class.entity";

@Entity({ name: 'study_year' })
export class StudyYearEntity {
	@PrimaryGeneratedColumn()
	@ApiProperty()
	id!: number;

	@ApiProperty()
	@Column('varchar', { length: '5', name: 'value' })
	value!: string;

	@OneToMany(() => ClassEntity, classes => classes.parallel)
	@ApiProperty({ type: () => ClassEntity})
	public classes!: ClassEntity[];

	@ApiProperty()
	@CreateDateColumn({ type: 'timestamp', name: 'created_at' })
	readonly createdAt!: Date;
  
	@ApiProperty()
	@UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
	readonly updatedAt!: Date;
}