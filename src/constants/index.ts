export enum UserRole {
	Admin = 'admin'
}

export type EducationLevel = 
	| 'elementary'
	| 'middle'
	| 'highSchool'

export class Constants {
	public static EducationLevel: EducationLevel[] = [
			'elementary',
			'middle',
			'highSchool'
	]
	
	public static UserRole: UserRole[] = [
		UserRole.Admin
	];
}