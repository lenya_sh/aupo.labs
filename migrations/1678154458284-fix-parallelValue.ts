import {MigrationInterface, QueryRunner} from "typeorm";

export class fixParallelValue1678154458284 implements MigrationInterface {
    name = 'fixParallelValue1678154458284'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX \`IDX_b31cab83ca9ee230518fcf1f2c\` ON \`parallels\``);
        await queryRunner.query(`ALTER TABLE \`parallels\` DROP COLUMN \`parallel_value\``);
        await queryRunner.query(`ALTER TABLE \`parallels\` ADD \`parallel_value\` varchar(3) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`parallels\` ADD UNIQUE INDEX \`IDX_b31cab83ca9ee230518fcf1f2c\` (\`parallel_value\`)`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`parallels\` DROP INDEX \`IDX_b31cab83ca9ee230518fcf1f2c\``);
        await queryRunner.query(`ALTER TABLE \`parallels\` DROP COLUMN \`parallel_value\``);
        await queryRunner.query(`ALTER TABLE \`parallels\` ADD \`parallel_value\` int NOT NULL`);
        await queryRunner.query(`CREATE UNIQUE INDEX \`IDX_b31cab83ca9ee230518fcf1f2c\` ON \`parallels\` (\`parallel_value\`)`);
    }

}
