import {MigrationInterface, QueryRunner} from "typeorm";

export class AddRelationParallelAndEducationLevel1678385531978 implements MigrationInterface {
    name = 'AddRelationParallelAndEducationLevel1678385531978'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`parallels\` ADD \`education_level_id\` int NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`parallels\` ADD CONSTRAINT \`FK_8eae6d677258f661a45c84f526d\` FOREIGN KEY (\`education_level_id\`) REFERENCES \`education_level\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`parallels\` DROP FOREIGN KEY \`FK_8eae6d677258f661a45c84f526d\``);
        await queryRunner.query(`ALTER TABLE \`parallels\` DROP COLUMN \`education_level_id\``);
    }

}
