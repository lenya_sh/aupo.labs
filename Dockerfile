FROM node:14.17.0-alpine

WORKDIR /var/www/app

ADD package.json package-lock.json ./

# RUN apk --no-cache add python make g++ git

ENV UNO_URL https://raw.githubusercontent.com/dagwieers/unoconv/master/unoconv

ENV CHROME_BIN="/usr/bin/chromium-browser" \
    PUPPETEER_SKIP_CHROMIUM_DOWNLOAD="true"

RUN apk --no-cache add bash mc \
            curl \
            util-linux \
            libreoffice-common \
            libreoffice-writer \
            ttf-droid-nonlatin \
            ttf-droid \
            ttf-dejavu \
            ttf-freefont \
            ttf-liberation \
        && curl -Ls $UNO_URL -o /usr/local/bin/unoconv \
        && chmod +x /usr/local/bin/unoconv \
        && ln -s /usr/bin/python3 /usr/bin/python \
        && apk del curl \
        && rm -rf /var/cache/apk/*

RUN npm ci

ADD .eslintrc.js nest-cli.json tsconfig.json ormconfig.ts tsconfig.build.json  ./

ADD .env /var/www/app/.env

CMD [ "npm", "run", "start:dev", "--preserveWatchOutput" ]

EXPOSE 3000