dcup:
	docker-compose down && docker-compose up --build

dcdown:
	docker-compose down

dcsh:
	docker exec -it app_backend sh