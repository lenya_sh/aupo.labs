import * as path from 'path';

module.exports = {
    type: 'mysql',
    host: process.env['MYSQL_HOST'],
    port: Number(process.env['MYSQL_PORT']),
    username: process.env['MYSQL_USER'],
    password: process.env['MYSQL_PASSWORD'],
    database: process.env['MYSQL_DATABASE'],
    synchronize: false,
    logging: true,
    entities: [
        'src/**/*.entity.ts'
    ],
    migrations: [
         'migrations/**/*.ts'
    ],
    cli: {
        migrationsDir: path.join(__dirname, "migrations")
    }
};